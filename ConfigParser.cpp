#include "ConfigParser.h"

#include "Conmon.h"

#include <fstream>

#include <string>

#include <boost/tokenizer.hpp>

#include <boost/foreach.hpp>



using namespace boost;

	

ConfigParser::ConfigParser(const string& filename)

{

	ifstream configFile(filename);

	if(configFile.good())

	{

		while(!configFile.eof()) {

			string line;

			getline(configFile, line);

			if(!line.empty()) 

			{

				typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

				boost::char_separator<char> sep("=,");

				tokenizer tokens(line, sep);

				vector<std::string> elements;

				BOOST_FOREACH(const string& t, tokens)

				{

					elements.push_back(t);

				}



				if(elements.size() != 3)

				{

					throw ParsingException();

				}



				if(elements[0] == "program")

				{

					hierarchyPidMap[atoi(elements[1].c_str())] = static_cast<Hierarchy>(atoi(elements[2].c_str()));

				}

			}

		}

	}

	else

	{

		throw NotfoundException();

	}		

}



ConfigParser::Hierarchy ConfigParser::getHierarchy(unsigned int pid)

{

	if(hierarchyPidMap.find(pid) != hierarchyPidMap.end()) 

	{

		return hierarchyPidMap[pid];

	}

	else 

	{

		return INVALID;

	}

}



