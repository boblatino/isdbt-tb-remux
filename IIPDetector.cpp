#include "IIPDetector.h"

#include <assert.h>

#include "IIPStructs.h"

#include "IIPParser.h"

#include "ISDBTInfoParser.h"

#include "TSParser.h"

#include "PATParser.h"

#include "PMTParser.h"



#include <direct.h>

#include <stdlib.h>

#include <stdio.h>

#include <algorithm>

#include <set>



FileParser::FileParser(): tsFile("test.trp", ios::binary | ios::in)

{

   char* buffer;



   // Get the current working directory: 

   if( (buffer = _getcwd( NULL, 0 )) == NULL )

      perror( "_getcwd error" );

   else

   {

      printf( "%s \nLength: %d\n", buffer, strlen(buffer) );

      free(buffer);

   }

	

	startDecode();

}

	

FileParser::~FileParser()

{

	

}



		

void FileParser::startDecode()

{

	if (tsFile.is_open())

	{

		streampos syncBytePos = searchForIIPByte(0);

	}

	else 

	{

		throw NotfoundException();

	}

}



string binary( unsigned long n )

{

string result;



do result.push_back( '0' + (n & 1) );

while (n >>= 1);



reverse( result.begin(), result.end() );

cout << result.c_str() << endl;

return result;

}



// Si convierto de char a int DEBE SER: unsigned char a int!!



streampos FileParser::searchForIIPByte(const streampos& offset)

{

	//Set the file to the appropiate offset

	//tsFile.seekg(offset);

	unsigned int packetCount = 0;

	unsigned int layerACount = 0;

	unsigned int layerBCount = 0;

	unsigned int layerCCount = 0;

    unsigned int nullCount = 0;

	

	std::set<int> pidsOnA;

	std::set<int> pidsOnB;



	PATParser patParser;

	std::set<ProgramInformation> programs;



	// pid -- Count

	std::map<int, size_t> pids;

	bool frameToggle = false;

	unsigned int iipCount = 2644;



	while (!tsFile.eof())

	{

		packetCount++;

		unsigned char tsHeader[4] = {0};

		tsFile.read(tsHeader, 4);

		TSParser tsParser;

		tsParser.read(tsHeader);

		unsigned char tsHeaderTest[4] = {0};

		tsParser.write(tsHeaderTest);

		assert(memcmp(tsHeader, tsHeaderTest, sizeof(unsigned char) * 4) == 0);



		//assert(tsParser.mHeader.payload_start_indicator == 0);

		assert(tsParser.mHeader.sync_byte == 0x47);

		assert(tsParser.mHeader.transport_error_indicator == 0);

		assert(tsParser.mHeader.transport_scrambling_control == 0);

		

		if(pids.count(tsParser.mHeader.PID) > 0) {

			pids[tsParser.mHeader.PID] = (pids[tsParser.mHeader.PID] + 1) & 0xF;

		} else {

			size_t countStart = 8;

			if(tsParser.mHeader.PID == 290) {

				countStart = 9;

			}

			if(tsParser.mHeader.PID == 529) {

				countStart = 14;

			}

			if(tsParser.mHeader.PID == 306) {

				countStart = 15;

			}

			if(tsParser.mHeader.PID == 530) {

				countStart = 6;

			}

			if(tsParser.mHeader.PID == 0) {

				countStart = 13;

			}

			if(tsParser.mHeader.PID == 288) {

				countStart = 0;

			}

			if(tsParser.mHeader.PID == 304) {

				countStart = 0;

			}

			if(tsParser.mHeader.PID == 259) {

				countStart = 3;

			}

			if(tsParser.mHeader.PID == 258) {

				countStart = 5;

			}

			if(tsParser.mHeader.PID == 8136) {

				countStart = 5;

			}

			if(tsParser.mHeader.PID == 8176) {

				countStart = 9;

			}

			if(tsParser.mHeader.PID == 288) {

				countStart = 0;

			}

			if(tsParser.mHeader.PID == 528) {

				countStart = 0;

			}

			if(tsParser.mHeader.PID == 16) {

				countStart = 9;

			}

			if(tsParser.mHeader.PID == 36) {

				countStart = 9;

			}

			if(tsParser.mHeader.PID == 17) {

				countStart = 5;

			}

			if(tsParser.mHeader.PID == 20) {

				countStart = 9;

			}

			pids[tsParser.mHeader.PID] = countStart;

		}



		// NULL 

		if((tsParser.mHeader.PID != 8191) && (tsParser.mHeader.PID != 288) && (tsParser.mHeader.PID != 304) && (tsParser.mHeader.PID != 528)) {

			assert(tsParser.mHeader.continuity_counter == pids[tsParser.mHeader.PID]); 

		}	

		



		// If there is an error, skip

		if(tsParser.mHeader.transport_error_indicator == 0x1) {

			tsFile.seekg(204 - 4);

			//iipCount++;

			continue;

		}



		// Skip if this has only adaptation field

		if (tsParser.mHeader.adaption_field_control == 0x2) {

			tsFile.seekg(204 - 4);

			iipCount++;

			continue;

		}

/*

		// Skip if the adaptation field (this mode has adapt field + payload)

		if (tsParser.mHeader.adaption_field_control == 0x3) {

			tsFile.seekg(204 - 4);

			//tsFile.seekg(tsParser.mHeader.adaption_field_length);

			continue;

		}*/





		//Read 184 bytes to get the payload

		unsigned char payload[184] = {0};

		tsFile.read(payload, 184);

			

		unsigned char isdbtInfoBuffer[8] = {0};

		tsFile.read(isdbtInfoBuffer, 8); 



		ISDBTInfoParser isdbtParser;

		isdbtParser.read(isdbtInfoBuffer);



		unsigned char isdbtInfoBufferTest[8] = {0};

		isdbtParser.write(isdbtInfoBufferTest);

		assert(memcmp(isdbtInfoBuffer, isdbtInfoBufferTest, sizeof(unsigned char) * 8) == 0);



		unsigned char parity[8] = {0};

		tsFile.read(parity, 8);



		unsigned char layer = isdbtParser.mIsdbtInfo.layer_indicator;			



		//Cualquiera de estas capas A,B,C trae en payload el video

		//Capa A

		if(tsParser.mHeader.PID == 0x1FFF) {  //Null packet so discard

			nullCount++;

			iipCount++;

			continue;

		}



		// Parse PAT

		if(tsParser.mHeader.PID == 0x0) {  

			unsigned char* start = (unsigned char*)payload;

			if(tsParser.mHeader.payload_start_indicator == 0x1) {

				// Skip the pointer

					start += 1;			

			}

			if(patParser.isFilled() == false) {

				patParser.read(start);

			}		

		}



		// Parse PMTs

		if(patParser.isFilled()) {

			const std::map<unsigned int, unsigned int>& programToPid = patParser.mProgramNumberToMapPID;

			std::map<unsigned int, unsigned int>::const_iterator it = programToPid.find(tsParser.mHeader.PID);

			if(it != programToPid.end()) {  

				unsigned char* start = (unsigned char*)payload;

				if(tsParser.mHeader.payload_start_indicator == 0x1) {

					// Skip the pointer

					start += 1;			

				}

				PMTParser pmtParser(it->second);

				pmtParser.read(start);

				programs.insert(programs.end(), pmtParser.mProgramInformation);

			}

		}

			

			

		if(layer == 0x1) {

			layerACount++;

			pidsOnA.insert(pidsOnA.begin(), tsParser.mHeader.PID);

			//assert(tsParser.mHeader.adaption_field_control == 3);

		}



		//Capa B

		if(layer == 0x2) {

			layerBCount++;

			pidsOnB.insert(pidsOnB.begin(), tsParser.mHeader.PID);

			//assert(tsParser.mHeader.adaption_field_control == 3);

		}



		//Capa C

		if(layer == 0x3) {

			layerCCount++;

		}



		ISDB_information& isdbtInfo = isdbtParser.mIsdbtInfo;

		assert(isdbtInfo.TMCC_identifier == 2);

		assert(isdbtInfo.reserved == 1);

		assert(isdbtInfo.buffer_reset_control_flag == 0);

		assert(isdbtInfo.switch_on_control_flag_for_emergency_broadcasting == 0);

		assert(isdbtInfo.initialization_timing_head_packet_flag == 0);

		assert(isdbtInfo.frame_head_packet_flag == 0);

		assert(isdbtInfo.frame_indicator == (unsigned char)frameToggle); // Toggles each iip

		assert(isdbtInfo.count_down_index == 15);

		assert(isdbtInfo.AC_data_invalid_flag == 1);

		assert(isdbtInfo.AC_data_effective_bytes == 3);

		//assert(isdbtInfo.TSP_counter == iipCount); // increases each packet and resets to 0 each iip (end 4607 in the test)

		assert(isdbtInfo.stuffing_bit_or_AC_data == std::numeric_limits<unsigned int>::max());

					

			

		// El paquete trae solo informacion ISDBT en el payload y no video

		if(layer == 0x8) {

			static unsigned char count = 9;

			assert(tsParser.mHeader.payload_start_indicator == 1);

			assert(tsParser.mHeader.adaption_field_control == 3 || tsParser.mHeader.adaption_field_control == 1);

			assert(tsParser.mHeader.transport_priority == 0);

			assert(tsParser.mHeader.continuity_counter == count);

			count = (count + 1) & 0xF;

			

			layerACount = 0;

			layerBCount = 0;

			layerCCount = 0;



			assert(tsParser.mHeader.PID == 0x1FF0); // Verificado con software externo



			assert(isdbtInfo.layer_indicator == 8); 

			assert(isdbtInfo.TSP_counter == 4607);



			//iip

			IIPParser parser;

			parser.read(payload);

			

			assert(parser.mIip.IIP_packet_pointer == 0);

			static bool tog = false;

			assert(parser.mIip.mcci.TMCC_synchronization_word == (unsigned char)tog); 

			tog = !tog;

			assert(parser.mIip.mcci.AC_data_effective_position == 1); 

			assert(parser.mIip.mcci.reserved == 3); 



			assert(parser.mIip.mcci.mgi.initialization_timing_indicator == 15); 

			assert(parser.mIip.mcci.mgi.current_mode == 3);

			assert(parser.mIip.mcci.mgi.current_guard_interval == 2);

			assert(parser.mIip.mcci.mgi.next_mode == 3);

			assert(parser.mIip.mcci.mgi.next_guard_interval == 2);



			assert(parser.mIip.mcci.tmccInformation.system_identifier == 0);

			assert(parser.mIip.mcci.tmccInformation.count_down_index == 15);

			assert(parser.mIip.mcci.tmccInformation.switch_on_control_flag_used_for_alert_broadcasting == 0);



			//Pierdo un bit a proposito y en la norma no figura que es!

			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerA.modulation_scheme == 1);

			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerA.coding_rate_of_inner_code == 1);

			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerA.length_of_time_interleaving == 2);

			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerA.number_of_segments == 1);



			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerB.modulation_scheme == 3);

			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerB.coding_rate_of_inner_code == 2);

			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerB.length_of_time_interleaving == 3);

			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerB.number_of_segments == 12);



			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerC.modulation_scheme == 7);

			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerC.coding_rate_of_inner_code == 7);

			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerC.length_of_time_interleaving == 7);

			assert(parser.mIip.mcci.tmccInformation.currentConfiguration.layerC.number_of_segments == 15);



			//Next configuration information

			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.partial_reception_flag == 1);



			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerA.modulation_scheme == 1);

			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerA.coding_rate_of_inner_code == 1);

			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerA.length_of_time_interleaving == 2);

			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerA.number_of_segments == 1);



			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerB.modulation_scheme == 3);

			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerB.coding_rate_of_inner_code ==	 2);

			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerB.length_of_time_interleaving == 3);

			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerB.number_of_segments == 12);



			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerC.modulation_scheme == 7);

			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerC.coding_rate_of_inner_code == 7);

			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerC.length_of_time_interleaving == 7);

			assert(parser.mIip.mcci.tmccInformation.nextConfiguration.layerC.number_of_segments == 15);

			

			//phase_shift_correction_value No dice q carajo es

			assert(parser.mIip.mcci.tmccInformation.phase_correction_of_CP_in_connected_transmission == 7);

			// Reservado para futuro 12

			assert(parser.mIip.mcci.tmccInformation.TMCC_reserved_future_use == 4095);

			//Reservado para futuro 10

			assert(parser.mIip.mcci.tmccInformation.reserved_future_use == 1023);



			//assert(parser.mIip.mcci.CRC_32 == 0);



			//IIP branch number: Indica el n�mero de ramales del IIP

			assert(parser.mIip.IIP_branch_number == 0);



			//Last iip branch number: Indica el �ltimo �IIP_branch_number� del sub-paquete assert(parser.mIip.

			assert(parser.mIip.last_IIP_branch_number == 0);

			assert(parser.mIip.network_synchronization_information_lenght == 1);

			// El valor anterior indica que el len del net sync information es 1 es decir q hay uno solo

			assert(parser.mIip.nsi.synchronization_id == 255);

			// Stuffin byte (basicamente relleno ya q no hay info de FU)				

			// 128 bits deberia ser el relleno si es que es la suma total de FU

			/*size_t count = 0;

			for(size_t i = 26 ; i < 26+16 ; i++) {

				assert(parser.mIip.nsi.stuffing_byte[count] == 0);

				count++;

			}	*/



			unsigned char readBuff[184];

			parser.write(readBuff);			



			frameToggle = !frameToggle;

			iipCount = 0;

		}

		iipCount++;

	}

	return 0;

}