#pragma once
#include <string>
#include <vector>
#include "Conmon.h"
#include "FileBuffer.h"

using namespace std;

class FileParser
{
public:
	FileBuffer tsFile;

	void startDecode();
	streampos fileSize();
	streampos searchForIIPByte(const streampos& offset);
	FileParser();
	~FileParser();



};