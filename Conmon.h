#pragma once
#include <iostream>
#include "Exceptions.h"
#include <map>

#define LOG(x) std::cout << x << std::endl
#define TS_PACKET_SIZE 188
#define ISDBT_PACKET_SIZE 204
#define ASSERT_THROW(expression, except) if(!(expression)) throw except 

//#define PRINT_TS 5

/**********************************************************************************************//**
 * \struct	TS_header
 *
 * \brief	Representa la informacion de la cabecera del paquete TS
 *
 * \author	Ramiro
 * \date	30/06/2011
 **************************************************************************************************/
struct AdaptationField {
	unsigned char discontinuity_indicator;
};

struct TS_header
{
	unsigned char sync_byte;
	unsigned char transport_error_indicator;
	unsigned char payload_start_indicator;
	unsigned char transport_priority;
	unsigned int PID;
	unsigned char transport_scrambling_control;
	unsigned char adaption_field_control;
	unsigned char continuity_counter;
	unsigned char adaption_field_length; // if any
	AdaptationField adaptation_field;
};

enum STREAM_TYPE {
	MPEG1_VIDEO,
	MPEG2_VIDEO,
	MPEG1_AUDIO,
	MPEG2_AUDIO,
	PRIVATE_SECTION,
	TELETEXT_SUBS,
	AAC_AUDIO,
	PRIVATE_STREAM,
	AC3_DTS_AUDIO,
	DTS_AUDIO,
	OTHER
};

struct ProgramInformation {
	bool operator< (const ProgramInformation& other) const {
		if(program_number == other.program_number) {
			return false;
		}
		if(program_number < other.program_number) {
			return true;
		}
		return false;
	}
	unsigned int program_number; 
	std::map<unsigned int, STREAM_TYPE> pidAndType;
};

struct ConfigRead{
	int mode;
	int codeRate[3];
	int modulation[3];
	int segments[3];
	int guard;

	bool isA;
	bool isB;
	bool isC;
	size_t fileSize;
};