#pragma once
#include <string>
#include <vector>
#include "Conmon.h"
#include <fstream>

using namespace std;

/**********************************************************************************************//**
 * \class	TSdecode
 *
 * \brief	Clase que decodifica el stream MPEG TS
 *
 **************************************************************************************************/

class TSdecode
{
public:	

	/**********************************************************************************************//**
	 * \fn	TSdecode::TSdecode(const string& fileName);
	 *
	 * \brief	Constructor
	 *
	 * \param	fileName	Nombre del archivo a decodificar
	 **************************************************************************************************/

	TSdecode(const string& fileName);
	~TSdecode();

	/**********************************************************************************************//**
	 * \fn	ifstream& TSdecode::getRawFile();
	 *
	 * \brief	Obtiene el archivo TS
	 *
	 * \return	Handler al archivo TS
	 **************************************************************************************************/

	ifstream& getRawFile();		

	/**********************************************************************************************//**
	 * \fn	void TSdecode::startDecode();
	 *
	 * \brief	Inicia la decodificacion del archivo
	 *
	 **************************************************************************************************/

	void startDecode();

	/**********************************************************************************************//**
	 * \fn	streampos TSdecode::fileSize();
	 *
	 * \brief	Obtiene el tamanio del archivo
	 *
	 * \return	Tamanio del archivo
	 **************************************************************************************************/

	streampos fileSize();

	/**********************************************************************************************//**
	 * \fn	void TSdecode::displayTransportHeaders();
	 *
	 * \brief	Imprime en pantalla los N headers del TS
	 *
	 **************************************************************************************************/

	void displayTransportHeaders();	

    /**********************************************************************************************//**
     * \fn	unsigned int TSdecode::getPid(size_t index);
     *
     * \brief	Obtiene el PID a partir de un indice
     *
     * \param	index   Indice del pid
     *
     * \return   pid.
     **************************************************************************************************/

    unsigned int getPid(size_t index);

private:

	/**********************************************************************************************//**
	 * \fn	streampos TSdecode::searchForSyncByte(const streampos& offset);
	 *
	 * \brief	Busca el byte de sincronismo del TS
	 *
	 * \param	offset	Offset del archivo
	 *
	 * \return	Posicion del bit de sincronismo
	 **************************************************************************************************/

	streampos searchForSyncByte(const streampos& offset);

	/**********************************************************************************************//**
	 * \fn	void TSdecode::TSheaderDecode(TS_header& header);
	 *
	 * \brief	Decodifica el header TS
	 *
	 * \param [in,out]	header TS
	 **************************************************************************************************/

	void TSheaderDecode(TS_header& header);

	///< Handler a archivo TS
	ifstream tsFile;

	///< Headers TS
	vector<TS_header> headers;
};