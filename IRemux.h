#pragma once
#include <string>
#include "Exceptions.h"
#include <set>
#include "Conmon.h"
#include "GuiToBTS.h"

struct ISDBTStatics {
	unsigned int layerApacketCount;
	unsigned int layerBpacketCount;
	unsigned int layerCpacketCount;
	unsigned int nullPacketCount;
	unsigned int iipPacketCount;
};

enum FILE_TYPE {
	BTS,
	TS,
	INVALID
};

struct ISDBTParseResult {
	ConfigRead config;
	ISDBTStatics stats;
};

struct IRemux {
	
	// will throw errors
	virtual void parseISDBTFile(ISDBTParseResult& result) = 0;
	virtual void convertToTS(const std::string& writeFileName) = 0;
	virtual void parseTs(std::set<ProgramInformation>& programs) = 0;
	virtual void convertToBTS(const GuiInfo& guiInfo, const std::string& writeFileName) = 0;
}; 
