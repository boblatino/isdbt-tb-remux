#pragma once

#include "IIPStructs.h"
//#include "ReedSolomon/ReedSolomon.h"

class ISDBTInfoParser
{
public:
	ISDBTInfoParser(){}
	void read(unsigned char* aBuffer) 
	{
		mIsdbtInfo.TMCC_identifier = (aBuffer[0] & 0xC0) >> 6;
		mIsdbtInfo.reserved = (aBuffer[0] & 0x20) >> 5;
		mIsdbtInfo.buffer_reset_control_flag = (aBuffer[0] & 0x10) >> 4;
		mIsdbtInfo.switch_on_control_flag_for_emergency_broadcasting = (aBuffer[0] & 0x8) >> 3;
		mIsdbtInfo.initialization_timing_head_packet_flag = (aBuffer[0] & 0x4) >> 2;
		mIsdbtInfo.frame_head_packet_flag = (aBuffer[0] & 0x2) >> 1;
		mIsdbtInfo.frame_indicator = (aBuffer[0] & 0x1);
		mIsdbtInfo.layer_indicator = (aBuffer[1] & 0xF0) >> 4;
		mIsdbtInfo.count_down_index = aBuffer[1] & 0xF;
		mIsdbtInfo.AC_data_invalid_flag = (aBuffer[2] & 0x80) >> 7;
		mIsdbtInfo.AC_data_effective_bytes = (aBuffer[2] & 0x60) >> 5;
		mIsdbtInfo.TSP_counter = (aBuffer[2] & 0x1F) << 8 | aBuffer[3];
		mIsdbtInfo.stuffing_bit_or_AC_data = aBuffer[4] << 24 | aBuffer[5] << 16 | aBuffer[6] << 8 | aBuffer[7];
		memcpy(mIsdbtInfo.reed_solomon, &(aBuffer[8]), 8);
	}

	void write(unsigned char* aBuffer) 
	{
		aBuffer[0] = mIsdbtInfo.TMCC_identifier << 6 | mIsdbtInfo.reserved << 5 | mIsdbtInfo.buffer_reset_control_flag << 4 |
			         mIsdbtInfo.switch_on_control_flag_for_emergency_broadcasting << 3 | mIsdbtInfo.initialization_timing_head_packet_flag << 2 |
					 mIsdbtInfo.frame_head_packet_flag << 1 | mIsdbtInfo.frame_indicator;
		
		aBuffer[1] = mIsdbtInfo.layer_indicator << 4 | mIsdbtInfo.count_down_index;
		
		aBuffer[2] = mIsdbtInfo.AC_data_invalid_flag << 7 | mIsdbtInfo.AC_data_effective_bytes << 5 | (mIsdbtInfo.TSP_counter & 0x1F00) >> 8;

		aBuffer[3] = mIsdbtInfo.TSP_counter & 0xFF;

	    aBuffer[4] = (mIsdbtInfo.stuffing_bit_or_AC_data & 0xFF000000) >> 24; 

		aBuffer[5] = (mIsdbtInfo.stuffing_bit_or_AC_data & 0xFF0000) >> 16;

		aBuffer[6] = (mIsdbtInfo.stuffing_bit_or_AC_data & 0xFF00) >> 8;

		aBuffer[7] = (mIsdbtInfo.stuffing_bit_or_AC_data & 0xFF);
	}


//private:
	ISDB_information mIsdbtInfo;
};