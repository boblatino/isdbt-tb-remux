#pragma once
#include "IRemux.h"
#include "Remux.h"
#include <memory>
#include "Gui/RemuxNotifier.h"

struct CallInfo;

template <class T>
class BlockingQueue;

namespace boost{
	class thread;
}

class RemuxDecoupleThread : public IRemux {
	
public:
	RemuxDecoupleThread(IRemux& remux, RemuxNotifier& notifier); 

	void operator()();	
	virtual void parseISDBTFile( ISDBTParseResult& result ); 
	virtual void convertToTS( const std::string& writeFileName ); 
	virtual void parseTs( std::set<ProgramInformation>& programs ) ;
	virtual void convertToBTS( const GuiInfo& guiInfo, const std::string& writeFileName ) ;
	FILE_TYPE getFileType() const { return static_cast<Remux*>(&mRemux)->getFileType(); }
	const std::string& getFilename() const {  return static_cast<Remux*>(&mRemux)->getFilename(); }

private:
	RemuxDecoupleThread(const RemuxDecoupleThread&);
	RemuxDecoupleThread& operator= (const RemuxDecoupleThread&);

	IRemux& mRemux;
	std::auto_ptr<boost::thread> mThread;
	std::auto_ptr<BlockingQueue<CallInfo> > mQueue;
	RemuxNotifier& mNotifier;
};