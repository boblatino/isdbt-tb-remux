#pragma once
#include <map>
#include <string>
using namespace std;

/**********************************************************************************************//**
 * \class	ConfigParser
 *
 * \brief	Parser del archivo de confuguracion 
 *
 * El mismo debe estar en el siguiente formato:
 * program=PID,Jerarquia
 * Donde Jerarquia 0 corresponde al tipo A, 1 al tipo B, 2 al tipo C
 * Ejemplo:
 * program=4479,0
 * program=4228,1
 * 
 **************************************************************************************************/
class ConfigParser
{
public:

	/**********************************************************************************************//**
	 * \fn	ConfigParser::ConfigParser(const string& filename);
	 *
	 * \brief	Constructor
	 *
	 * \param	filename	Nombre del archivo de configuracion
	 **************************************************************************************************/
	ConfigParser(const string& filename);	

	/**********************************************************************************************//**
	 * \enum	Hierarchy
	 *
	 * \brief	Valores que representan las distintas jerarquias de ISDBT. 
	 **************************************************************************************************/
	enum Hierarchy
	{
		A,
		B,
		C,
		INVALID
	};

	/**********************************************************************************************//**
	 * \fn	Hierarchy ConfigParser::getHierarchy(unsigned int pid);
	 *
	 * \brief	Metodo para obtener el tipo de jerarquia configurada
	 *
	 * \param	pid	El PID.
	 *
	 * \return	Tipo de jerarquia
	 **************************************************************************************************/

	Hierarchy getHierarchy(unsigned int pid);	

private:
	///< Mapa en el cual se guarda el mapeo de PID vs tipo de jerarquia
	map<unsigned int, Hierarchy> hierarchyPidMap;
};