
#include <cstddef>
#include <iostream>
#include <string>
#include <fstream>
#include <assert.h>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread/thread.hpp>
#include <boost/threadpool.hpp>

#include "../ShifraHeaders/schifra_galois_field.hpp"
#include "../ShifraHeaders/schifra_galois_field_polynomial.hpp"
#include "../ShifraHeaders/schifra_sequential_root_generator_polynomial_creator.hpp"
#include "../ShifraHeaders/schifra_reed_solomon_encoder.hpp"
#include "../ShifraHeaders/schifra_reed_solomon_decoder.hpp"
#include "../ShifraHeaders/schifra_reed_solomon_block.hpp"
#include "../ShifraHeaders/schifra_erasure_channel.hpp"
#include "../ShifraHeaders/schifra_ecc_traits.hpp"
#include "../ShifraHeaders/schifra_utilities.hpp"

template<typename Encoder>
class erasure_process
{
public:
   erasure_process(const unsigned int& process_id,
                   boost::shared_ptr<Encoder>& encoder,
				   unsigned char* data)
   : process_id_(process_id),
     total_time_(0.0),
     encoder_(encoder),
     mData(data){
		 std::cout << mData[0] << std::endl;
   }

   erasure_process& operator=(const erasure_process& ep)
   {
      process_id_ = ep.process_id_;
      total_time_ = ep.total_time_;
   }

   double time() { return total_time_; }

   void execute()
   {
      const std::size_t code_length = Encoder::trait::code_length;
      const std::size_t fec_length  = Encoder::trait::fec_length;
      const std::size_t data_length = Encoder::trait::data_length;
      const std::size_t data_size   = data_length;

      typedef schifra::reed_solomon::block<code_length,fec_length> block_type;

      block_type block_stack;

      schifra::utils::timer timer;
      total_time_ = 0.0;
/*
	  unsigned char testMem[204];
	  memcpy(testMem, mData, 204);
	  
	  for(size_t i = 196; i < 204; ++i){
		  mData[i] = 0xFF;
	  }
	  */
	schifra::reed_solomon::copy<unsigned char,code_length,fec_length>(mData, block_stack);

	timer.start();

	
	if (!encoder_->encode(block_stack))
	{
		std::cout << "Error - Critical encoding failure!" << std::endl;
	}
	
	for(size_t i = 196 ; i < 204; ++i){
		mData[i] = block_stack[i];
	}
	
	//assert(memcmp(testMem, mData, 204) == 0);
      
	timer.stop();
	total_time_ += timer.time();
}

private:

   unsigned int process_id_;
   double total_time_;
   boost::shared_ptr<Encoder> encoder_;
   unsigned char* mData;
};


int run()
{
   std::ifstream file("test.trp", std::ios::binary);
   unsigned char buffer[204 * 4];
   file.read((char*)buffer, 204 * 4);
   assert(file.gcount() == (204 * 4));
	
	/* Finite Field Parameters */
    const std::size_t field_descriptor                 =   8;
	const std::size_t generator_polynommial_index      =   0;
	const std::size_t generator_polynommial_root_count =   8;

   /* Reed Solomon Code Parameters */
   const std::size_t code_length = 204;
   const std::size_t fec_length  = 8;

   schifra::galois::field field(field_descriptor,
	   schifra::galois::primitive_polynomial_size05,
	   schifra::galois::primitive_polynomial05);

   schifra::galois::field_polynomial generator_polynomial(field);
   schifra::sequential_root_generator_polynomial_creator(field,
														generator_polynommial_index,
														generator_polynommial_root_count,
														generator_polynomial);

   typedef schifra::reed_solomon::shortened_encoder<code_length,fec_length> encoder_type;
   typedef erasure_process<encoder_type> process_type;
   typedef boost::shared_ptr<process_type> process_ptr_type;

   const unsigned int max_thread_count = 4; // number of functional cores.

   std::vector<process_ptr_type> process_list;
   boost::threadpool::pool threadPool(4);
   for (unsigned int i = 0; i < max_thread_count; ++i)
   {
      unsigned char* data = buffer + (i * 204);
	  boost::shared_ptr<encoder_type> encoder(new encoder_type(field,generator_polynomial));
	  process_list.push_back(process_ptr_type(new process_type(i,encoder, data)));
	  threadPool.schedule(boost::bind(&process_type::execute, process_list[i]));
   }

   threadPool.wait();
   double time = -1.0;

   /* Determine the process with the longest running time. */
   for (std::size_t i = 0; i < process_list.size(); ++i)
   {
      time = ((time < process_list[i]->time()) ? process_list[i]->time() : time);
   }

   double mbps = (max_thread_count * 8.0 * code_length * (code_length - fec_length)) / (1048576.0 * time);

   std::cout << "Blocks decoded: " << max_thread_count * code_length << "\tTime: " << time <<"sec\tRate: " << mbps << "Mbps" << std::endl;

   return 0;
}
