#include "extended_field.h"

namespace errctrl {

    void ExtendedField::create_poly_rep(const Poly<FieldElement>& poly) {
        FieldElement elem_tab(1, &f);
        Poly<FieldElement> temp(&elem_tab, 0);
        poly_rep.resize(q);
        for (unsigned int i = 1; i < q; i++) {
            poly_rep[i] = temp % poly;
            temp >>= 1;
            poly_rep[i].clean();
        }
        FieldElement zero_elem(0, &f);
        poly_rep[0] = Poly<FieldElement > (&zero_elem, 0);
    }

    FieldElement::value_type ExtendedField::add(const FieldElement& a, const FieldElement& b) const {
        if (a.getVal() == 0 || b.getVal() == 0) {
            return a.getVal() + b.getVal();
        }
        if (a.getVal() != 0 && b.getVal() == a_inv(a)) {
            return 0;
        }
        unsigned char result;
        if (a.getVal() > b.getVal()) {
            result = (b.getVal() + zech_logarithms[a.getVal() - b.getVal() + 1] - 1) % (q - 1) + 1;
        } else {
            result = (a.getVal() + zech_logarithms[b.getVal() - a.getVal() + 1] - 1) % (q - 1) + 1;
        }
        return result;
    }

    FieldElement::value_type ExtendedField::a_inv(const FieldElement& a) const {
        if (a.getVal() == 0) {
            return 0;
        }
        if (p == 2) {
            return a.getVal();
        }
        unsigned char result = (a.getVal() + (q - 1) / 2 - 1) % (q - 1) + 1;
        return result;
    }

    FieldElement::value_type ExtendedField::mul(const FieldElement& a, const FieldElement& b) const {
        if (a.getVal() == 0 || b.getVal() == 0) {
            return 0;
        }
        unsigned char result = 1 + ((a.getVal() + b.getVal() - 2) % (q - 1));
        return result;
    }

    FieldElement::value_type ExtendedField::mul(const FieldElement& a, int b) const {
        Poly<FieldElement> temp = poly_rep[a.getVal()];
        temp = temp * FieldElement(b % f.size(), const_cast<SimpleField *> (&f));
        unsigned char result;
        for (unsigned int i = 0; i < poly_rep.size(); i++) {

            if (temp == poly_rep[i]) {
                result = i;
                return result;
            }
        }
        throw std::runtime_error("Multiplication error");
        return result;
    }

    FieldElement::value_type ExtendedField::m_inv(const FieldElement& a)const {
        if (a.getVal() == 0) {
            throw new std::invalid_argument("Zero has no multiplicative inverse");
        }
        if (a.getVal() == 1) {
            return 1;
        }
        unsigned char result = q + 1 - a.getVal();
        return result;
    }

    std::ostream & operator <<(std::ostream& os, const ExtendedField& f) {
        for (unsigned int i = 0; i < f.size(); i++) {
            os << i << '\t' << f.poly_rep[i];

            os << '\t' << (int) f.zech_logarithms[i];
            os << '\n';
        }
        return os;
    }

    void ExtendedField::imamura_algorithm() {
        std::vector<zech_logarithm> nx;
        nx.resize(q);
        for (unsigned int j = 0; j < q; j++) {
            zech_logarithm result = poly_rep[j][poly_rep[j].size() - 1].getVal();
            for (unsigned int i = 0; i < poly_rep[j].size() - 1; i++) {
                result = result * p + poly_rep[j][poly_rep[j].size() - 2 - i].getVal();
            }
            nx[j] = result;
        }
        std::vector<zech_logarithm> nzx;
        nzx.resize(q);
        for (unsigned int j = 0; j < q; j++) {
            if (((nx[j] - (zech_logarithm) p + 1) % (zech_logarithm) p) == 0) {
                nzx[j] = nx[j] - (zech_logarithm) p + 1;
            } else {
                nzx[j] = nx[j] + 1;
            }
        }
        zech_logarithms.resize(q);
        for (unsigned int i = 0; i < q; i++) {
            for (unsigned int j = 0; j < q; j++) {
                if (nzx[i] == nx[j]) {
                    zech_logarithms[i] = (int) j - 1;
                    break;
                }
            }
        }
    }

    ExtendedField::ExtendedField(characteristic p_, extension_order m_, const Poly<FieldElement>& poly_) :
    SimpleField(p_) {
        m = m_;
        if (m == 0) {
            throw std::invalid_argument("Field extension cannot be 0");
        }
        q = (size_type) std::pow((double)p, (int)m);
        f = SimpleField(p);
        if (is_primitive(poly_, &f) == false) {
            throw std::invalid_argument("Generator polynomial is not primitive");
        }
        create_poly_rep(poly_);
        imamura_algorithm();
    }

    ExtendedField::~ExtendedField() {

    }

    ExtendedField::size_type ExtendedField::size() const {
        return q;
    }

}
