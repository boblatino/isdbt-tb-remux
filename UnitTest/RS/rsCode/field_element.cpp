#include "field_element.h"
#include "simple_field.h"

namespace errctrl {

    FieldElement::FieldElement(FieldElement::value_type val_, SimpleField* f_) : f(f_) {
        val = val_;
        if (val > f->size() - 1) {
            val = val % f->size() + 1;
        }
    }

    FieldElement::value_type FieldElement::getVal() const {
        return val;
    }

    const SimpleField* FieldElement::getField() const {
        return f;
    }

    FieldElement& FieldElement::operator+=(const FieldElement& b) {
        val = f->add(*this, b);
        return *this;
    }

    FieldElement& FieldElement::operator-=(const FieldElement& b) {
        FieldElement temp = b;
        temp = f->a_inv(b);
        val = f->add(*this, temp);
        return *this;
    }

    FieldElement& FieldElement::operator*=(const FieldElement& b) {
        val = f->mul(*this, b);
        return *this;
    }

    FieldElement & FieldElement::operator*=(const FieldElement::value_type& b) {
        val = f->mul(*this, b);
        return *this;
    }

    FieldElement& FieldElement::operator/=(const FieldElement& b) {
        FieldElement temp = b;
        temp = f->m_inv(b);
        val = f->mul(*this, temp);
        return *this;
    }

    std::ostream & operator<<(std::ostream& s, FieldElement o) {
        s << (unsigned int) o.getVal();
        return s;
    }

    FieldElement operator+(const FieldElement& a, const FieldElement& b) {
        FieldElement result(a);
        result += b;
        return result;
    }

    FieldElement operator-(const FieldElement& a, const FieldElement& b) {
        FieldElement result(a);
        result -= b;
        return result;
    }

    FieldElement operator*(const FieldElement& a, const FieldElement& b) {
        FieldElement result(a);
        result *= b;
        return result;
    }

    FieldElement operator*(const FieldElement& a, const FieldElement::value_type& b) {
        FieldElement result(a);
        result *= b;
        return result;
    }

    FieldElement operator*(const FieldElement::value_type& a, const FieldElement& b) {
        FieldElement result(b);
        result *= a;
        return result;
    }

    FieldElement operator/(const FieldElement& a, const FieldElement& b) {
        FieldElement result(a);
        result /= b;
        return result;
    }

    FieldElement FieldElement::operator-() const {
        FieldElement result(*this);
        result = f->a_inv(*this);
        return result;
    }

    bool operator==(const FieldElement& a, const FieldElement& b) {
        return a.getVal() == b.getVal();
    }

    bool operator==(const FieldElement::value_type& a, const FieldElement& b) {
        return a == b.getVal();
    }

    bool operator==(const FieldElement& a, const FieldElement::value_type& b) {
        return a.getVal() == b;
    }

    bool operator!=(const FieldElement& a, const FieldElement& b) {
        return a.getVal() != b.getVal();
    }

    bool operator!=(const FieldElement::value_type& a, const FieldElement& b) {
        return a != b.getVal();
    }

    bool operator!=(const FieldElement& a, const FieldElement::value_type& b) {
        return a.getVal() != b;
    }

    FieldElement pow(const FieldElement& a, int value) {
        FieldElement result(a);
        if (value == 0) {
            result = 1;
            return result;
        }
        if (value < 0) {
            result = a.getField()->m_inv(result);
            value = -value;
        }
        FieldElement rem = result;
        for (int i = 1; i < value; i++) {
            result = a.getField()->mul(result, rem);
        }
        return result;
    }

    FieldElement & FieldElement::operator=(const FieldElement::value_type& b) {
        val = b;
        return *this;
    }

    void FieldElement::setField(SimpleField* f_new) {
        f = f_new;
    }

}