#ifndef POLY_HPP
#define POLY_HPP

#include <vector>
#include <stdexcept>

namespace errctrl {


    //!  Polynomial class.

    /*!
     * Implementation of polynomial arithmetic which can be used for Galois fields.
     * This class is based on boost polynomial library: http://www.boost.org/doc/libs/1_36_0/boost/math/tools/polynomial.hpp .
     */



    template <class T>
    class Poly {
    private:
        //! Container for polynomial coefficients.
        std::vector<T> m_data;

        //! Division mode determines result from division - quotient or remainder of division.

        enum div_mode {
            DIVIDE, /*!< Result is quotient. */
            MODULO /*!< Result is remainder. */
        };

        //! Function performs division of two polynomials and the result can be quotient or remainder from division. Result is determined by parameter mode.

        /*!
         * \param dividend dividend.
         * \param divisor divisor.
         * \param mode mode determines result of function.
         * \return quotient or remainder from division.
         */
        static Poly<T> divide_modulo(const Poly<T>& dividend, const Poly<T>& divisor, div_mode mode) {
            int diff = dividend.degree() - divisor.degree();
            std::vector<T> m_data_temp;
            Poly<T> base(dividend);
            if (dividend.degree() >= divisor.degree()) {
                T dividend_temp = divisor[divisor.degree()];

                if (dividend_temp == 0) {
                    throw std::invalid_argument("Cannot divide by 0");
                }
                Poly<T> temp_base = divisor;

                temp_base >>= diff;

                Poly<T> temp = temp_base;
                for (int i = 0; i <= diff; i++) {
                    T result = base[base.degree()] / dividend_temp;
                    if (base.degree() != dividend.degree() - i) {
                        result = dividend[0];
                        result = 0;
                    }
                    m_data_temp.insert(m_data_temp.begin(), result);
                    temp *= result;
                    base -= temp;
                    temp_base = temp_base << 1;
                    temp = (temp_base);
                }
            }
            if (mode == MODULO) {

                return base;
            }
            if (m_data_temp.empty()) {
                T zero_elem = dividend[0];
                zero_elem = 0;
                m_data_temp.push_back(zero_elem);
            }
            return Poly<T > (m_data_temp.data(), m_data_temp.size() - 1);
        }


    public:
        //! Constructor.

        /*!
         * \param data_ array with coefficients of polynomial.
         * \param deg_ degree of polynomial.
         */
        Poly(const T* data_, unsigned deg_) : m_data(data_, data_ + deg_ + 1) {
        }
        //! Constructor with no arguments.

        Poly() {
        }
        //! Function removes needless zeroes from head of polynomial.

        void clean() {
            while (m_data[m_data.size() - 1] == 0 && m_data.size() > 1) {
                m_data.erase(m_data.end() - 1);
            }
        }

        //! Value type of polynomial coefficients.
        typedef typename std::vector<T>::value_type value_type;
        //! Size type of polynomial.
        typedef typename std::vector<T>::size_type size_type;

        //! Function returns size of polynomial (including leading zeroes).

        /*!
         * \return size of polynomial.
         */
        size_type size()const {
            return m_data.size();
        }
        //! Function returns degree of polynomial.

        /*!
         * \return degree of polynomial.
         */
        size_type degree()const {
            size_type deg_temp = size();
            while (deg_temp--) {

                if (deg_temp == 0) {

                    return 0;
                }
                if (m_data[deg_temp] != 0) {
                    return deg_temp;
                }
            }
            throw std::runtime_error("Wrong degree of polynomial");
            return 0;
        }

        //! Function returns ith coefficient of polynomial.

        /*!
         * \param i position of coefficient in polynomial.
         * \return ith coefficient of polynomial.
         */
        value_type & operator[](size_type i) {
            return m_data[i];
        }

        //! Function returns ith coefficient of polynomial.

        /*!
         * \param i position of coefficient in polynomial.
         * \return ith coefficient of polynomial.
         */
        const value_type & operator[](size_type i)const {
            return m_data[i];
        }

        //! Function adds two polynomials and returns sum of them.

        /*!
         * \param summand one of summands.
         * \return sum of two polynomials.
         */
        Poly<T> & operator +=(const Poly<T>& summand) {
            size_type s1 = (std::min)(m_data.size(), summand.size());
            for (size_type i = 0; i < s1; ++i)
                m_data[i] += summand[i];
            for (size_type i = s1; i < summand.size(); ++i)
                m_data.push_back(summand[i]);
            return *this;
        }


        //! Function subtracts one polynomial from second polynomial and returns difference of them.

        /*!
         * \param subtrahend subtrahend.
         * \return difference of two polynomials.
         */
        Poly<T> & operator -=(const Poly<T>& subtrahend) {
            size_type s1 = (std::min)(m_data.size(), subtrahend.size());
            for (size_type i = 0; i < s1; ++i) {
                m_data[i] -= subtrahend[i];
            }
            for (size_type i = s1; i < subtrahend.size(); ++i)
                m_data.push_back(-subtrahend[i]);
            return *this;
        }

        //! Function multiplies two polynomials and returns product of them.

        /*!
         * \param factor factor.
         * \return product of two polynomials.
         */

        Poly<T> & operator *=(const Poly<T>& factor) {
            Poly<T> base(*this);
            *this *= factor[0];
            for (size_type i = 1; i < factor.size(); ++i) {
                Poly<T> t(base);
                t *= factor[i];
                size_type s = size() - i;
                for (size_type j = 0; j < s; ++j) {
                    m_data[i + j] += t[j];
                }
                for (size_type j = s; j < t.size(); ++j)
                    m_data.push_back(t[j]);
            }
            return *this;
        }

        //! Function multiplies polynomial by constant.

        /*!
         * \param constant constant.
         * \return polynomial multiplied by constant.
         */
        Poly<T> & operator *=(const T& constant) {

            for (size_type i = 0; i < m_data.size(); ++i)
                m_data[i] *= constant;
            return *this;
        }
        //! Function divides polynomial by polynomial.

        /*!
         * \param divisor divisor.
         * \return quotient of division of polynomial by polynomial.
         */
        Poly<T> & operator /=(const Poly<T>& divisor) {
            *this = divide_modulo(*this, divisor, DIVIDE);
            return *this;
        }
        //! Function divides polynomial by constant.

        /*!
         * \param constant constant.
         * \return polynomial divided by constant.
         */
        Poly<T> & operator /=(const T& constant) {
            for (size_type i = 0; i < m_data.size(); ++i)
                m_data[i] /= constant;
            return *this;
        }
        //! Function calculates remainder of division polynomial by polynomial.

        /*!
         * \param divisor divisor.
         * \return remainder of division polynomial by polynomial.
         */
        Poly<T> & operator %=(const Poly<T>& divisor) {
            *this = divide_modulo(*this, divisor, MODULO);
            return *this;
        }

        //! Function shifts polynomial towards zero.

        /*!
         * \param value number of positions to be shifted.
         * \return shifted polynomial.
         */
        Poly<T> & operator<<=(const size_type value) {
            if (value > m_data.size()) {
                throw std::invalid_argument("Invalid shift value for polynomial");
            }
            m_data.erase(m_data.begin(), m_data.begin() + value);
            return *this;
        }
        //! Function shifts polynomial towards infinity.

        /*!
         * \param value number of positions to be shifted.
         * \return shifted polynomial.
         */
        Poly<T> & operator>>=(const size_type value) {
            T elem = m_data[0];
            elem = 0;
            m_data.insert(m_data.begin(), value, elem);
            return *this;
        }
        //! Function evaluates polynomial with use of Horner's rule.

        /*!
         * \param value value used to evaluate polynomial.
         * \return value of polynomial.
         */
        T evaluate(const T value) const {
            T result = m_data[m_data.size() - 1];
            for (unsigned int i = 0; i < m_data.size() - 1; i++) {
                result = result * value + (*this)[m_data.size() - 2 - i];
            }
            return result;
        }

        //! Function returns number of non-zero coefficients of polynomial.

        /*!
         * \return number of non-zero coefficients of polynomial.
         */
        unsigned int weight() const {
            int w = 0;
            for (int i = 0; i < size(); ++i) {
                if (m_data.at(i) != 0)
                    w++;
            }
            return w;
        }

        //! Function calculates derivative of polynomial.

        /*!
         * \param poly input polynomial.
         * \return derivative of input polynomial.
         */
        static Poly<T> derivative(const Poly<T>& poly) {
            Poly<T> result(poly);
            result = result << 1;
            for (unsigned int i = 0; i < result.size(); i++) {
                result[i] = result[i] * (i + 1);
            }
            return result;
        }

    };

    //! Function adds two polynomials.

    /*!
     * \param a first addend.
     * \param b second addend.
     * \return sum of two polynomials.
     */
    template <class T>
    inline Poly<T> operator +(const Poly<T>& a, const Poly<T>& b) {
        Poly<T> result(a);
        result += b;
        return result;
    }

    //! Function subtracts one polynomial from second polynomial.

    /*!
     * \param a minuend.
     * \param b subtrahend.
     * \return difference of two polynomials.
     */
    template <class T>
    inline Poly<T> operator -(const Poly<T>& a, const Poly<T>& b) {
        Poly<T> result(a);
        result -= b;
        return result;
    }

    //! Function multiplies polynomial by polynomial.

    /*!
     * \param a first factor.
     * \param b second factor.
     * \return product of two polynomials.
     */
    template <class T>
    inline Poly<T> operator *(const Poly<T>& a, const Poly<T>& b) {
        Poly<T> result(a);
        result *= b;
        return result;
    }

    //! Function multiplies polynomial by constant.

    /*!
     * \param a polynomial.
     * \param b constant.
     * \return polynomial multiplied by constant.
     */
    template <class T>
    inline Poly<T> operator *(const Poly<T>& a, const T& b) {
        Poly<T> result(a);
        result *= b;
        return result;
    }

    //! Function multiplies polynomial by constant.

    /*!
     * \param a constant.
     * \param b polynomial.
     * \return polynomial multiplied by constant.
     */
    template <class T>
    inline Poly<T> operator *(const T& a, const Poly<T>& b) {
        Poly<T> result(b);
        result *= a;
        return result;
    }

    //! Function divides polynomial by polynomial.

    /*!
     * \param a dividend.
     * \param b divisor.
     * \return quotient of division of polynomial by polynomial.
     */
    template <class T>
    inline Poly<T> operator /(const Poly<T>& a, const Poly<T>& b) {
        Poly<T> result(a);
        result /= b;
        return result;
    }

    //! Function divides polynomial by constant.

    /*!
     * \param a dividend.
     * \param b constant.
     * \return quotient of division of polynomial by constant.
     */
    template <class T>
    inline Poly<T> operator /(const Poly<T>& a, const T& b) {
        Poly<T> result(a);
        result /= b;
        return result;
    }

    //! Function divides polynomial by polynomial and returns remainder of division.

    /*!
     * \param a dividend.
     * \param b divisor.
     * \return remainder of division of polynomial by polynomial.
     */
    template <class T>
    inline Poly<T> operator %(const Poly<T>& a, const Poly<T>& b) {
        Poly<T> result(a);
        result %= b;
        return result;
    }

    //! Function shifts polynomial towards zero.

    /*!
     * \param a polynomial to be shifted.
     * \param value number of positions to be shifted.
     * \return shifted polynomial.
     */
    template <class T>
    inline Poly<T> operator <<(const Poly<T>& a, const typename Poly<T>::size_type value) {
        Poly<T> result(a);
        result <<= value;
        return result;
    }

    //! Function shifts polynomial towards infinity.

    /*!
     * \param a polynomial to be shifted.
     * \param value number of positions to be shifted.
     * \return shifted polynomial.
     */
    template <class T>
    inline Poly<T> operator >>(const Poly<T>& a, const typename Poly<T>::size_type value) {
        Poly<T> result(a);
        result >>= value;
        return result;
    }
    //! Function compares two polynomials.

    /*!
     * \param a first polynomial.
     * \param b second polynomial.
     * \return true if polynomials are equal, otherwise false.
     */
    template <class T>
    bool operator==(const Poly<T>& a, const Poly<T>& b) {
        if (a.degree() != b.degree()) {
            return false;
        }
        for (unsigned int i = 0; i <= a.degree(); i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    //! Function compares two polynomials.

    /*!
     * \param a first polynomial.
     * \param b second polynomial.
     * \return false if polynomials are equal, otherwise true.
     */
    template <class T>
    bool operator!=(const Poly<T>& a, const Poly<T>& b) {
        return !(a == b);
    }
    //! Function prints polynomial.

    /*!
     * \param os ostream object.
     * \param poly polynomial to be printed.
     * \return ostream object.
     */
    template <class charT, class traits, class T>
    inline std::basic_ostream<charT, traits> & operator <<(std::basic_ostream<charT, traits>& os, const Poly<T>& poly) {
        os << "{ ";
        for (unsigned i = 0; i < poly.size(); ++i) {
            if (i) os << ", ";
            os << poly[i];
        }
        os << " }";
        return os;
    }

}
#endif 



