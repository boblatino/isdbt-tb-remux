#ifndef EXTENDEDFIELD_H
#define EXTENDEDFIELD_H

#include <vector>
#include <cmath>
#include <iostream>
#include <exception>
#include <stdexcept>
#include "poly.h"
#include "simple_field.h"
#include "field_element.h"

namespace errctrl {


    //!  Field extension class.

    /*!
     * Implementation of extended Galois field arithmetic.
     */

    class ExtendedField : public SimpleField {
    private:
        //! Type of extension order.
        typedef unsigned int extension_order;
        //! Type of zech logarithm.
        typedef int zech_logarithm;
        //! Extension order of extended field.
        extension_order m;
        //! Size of extended field.
        size_type q;
        //! Zech logarithms of elements of extended field.
        std::vector<zech_logarithm> zech_logarithms;
        //! Polynomial representation of elements of extended field.
        std::vector<Poly<FieldElement> > poly_rep;
        //! Simple field which is base for extended field.
        SimpleField f;

        //! Function generates polynomial representation of elements of extended field.
        /*!
         * \param poly primitive polynomial - field generator.
         */
        void create_poly_rep(const Poly<FieldElement>& poly);
        //! Implementation of Imamura algorithm which is used to generate Zech logarithms of elements of extended field.
        void imamura_algorithm();

    public:
        //! Constructor.
        /*!
         * \param p_ characteristic of extended field.
         * \param m_ extension order.
         * \param poly_ primitive polynomial - field generator.
         */
        ExtendedField(characteristic p_, extension_order m_, const Poly<FieldElement>& poly_);
        //! Destructor.
        ~ExtendedField();
        //! Function adds two elements of field.
        /*!
         * \param a first summand.
         * \param b second summand.
         * \return sum of two elements.
         */
        FieldElement::value_type add(const FieldElement& a, const FieldElement& b) const;
        //! Function calculates additive inverse of element.
        /*!
         * \param a element whose additive inverse is returned.
         * \return additive inverse of input element.
         */
        FieldElement::value_type a_inv(const FieldElement& a) const;
        //! Function multiplies two elements of field.
        /*!
         * \param a first factor.
         * \param b second factor.
         * \return product of two elements.
         */
        FieldElement::value_type mul(const FieldElement& a, const FieldElement& b)const;
        //! Function multiplies element of field and integer.
        /*!
         * \param a element of field.
         * \param b an integer.
         * \return product of two elements.
         */
        FieldElement::value_type mul(const FieldElement& a, int b)const;
        //! Function calculates multiplicative inverse of element.
        /*!
         * \param a element whose multiplicative inverse is returned.
         * \return multiplicative inverse of input element.
         */
        FieldElement::value_type m_inv(const FieldElement& a)const;
        //! Function returns size of field - number of elements in field.
        /*!
         * \return size of field.
         */
        size_type size() const;
        //!  Function prints element.
        /*!
         * \param s ostream object.
         * \param o element to be printed.
         * \return ostream object.
         */
        friend std::ostream & operator <<(std::ostream& os, const ExtendedField& f);
    };

}
#endif