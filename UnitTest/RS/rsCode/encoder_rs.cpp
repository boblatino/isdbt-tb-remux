#include "encoder_rs.h"

namespace errctrl {

    void EncoderRS::create_generator() {
        FieldElement::value_type lt = (1 + b) % (f->size());
        FieldElement elems[2] = {FieldElement(lt, f), FieldElement(1, f)};
        elems[0] = f->a_inv(elems[0]);
        generator = Poly<FieldElement > (elems, 1);
        Poly<FieldElement> temp = generator;
        for (unsigned int i = 0; i < 2 * t - 1; i++) {
            temp[0] *= FieldElement(2, f);
            generator *= temp;
        }
    }

    Poly<FieldElement> EncoderRS::encode_sys(const Poly<FieldElement>& m) const {
        if ((m.size() + 2 * t) > f->size() - 1)
            throw std::invalid_argument("Wrong information polynomial size");
        Poly<FieldElement> result = m;
        result >>= 2 * t;
        result -= (result % generator);
        return result;
    }

    EncoderRS::EncoderRS(unsigned int t_, ExtendedField* f_, unsigned int b_) :
    b(b_), t(t_), f(f_) {
        create_generator();
    }

    Poly<FieldElement> EncoderRS::getGenerator() const {
        return generator;
    }

    unsigned int EncoderRS::getFirstPower() const {
        return b;
    }

    unsigned int EncoderRS::getErrCapability() const {
        return t;
    }

    const ExtendedField* EncoderRS::getExtendedField() const {
        return f;
    }

}