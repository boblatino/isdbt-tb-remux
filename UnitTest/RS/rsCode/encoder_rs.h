#ifndef ENCODER_RS_H
#define	ENCODER_RS_H
#include "field_element.h"
#include "extended_field.h"
#include "poly.h"

namespace errctrl {


    //!  Reed-Solomon encoder class.

    class EncoderRS {
    private:
        //! Code generator polynomial.
        Poly<FieldElement> generator;
        //! First power of primitive element used to calculate generator polynomial.
        unsigned int b;
        //! Error correction capability.
        unsigned int t;
        //! Extended field whose elements are code symbols.
        ExtendedField* f;
        //! Function calculates code generator polynomial.
        void create_generator();
    public:
        //! Constructor.
        /*!
         * \param t_ error correction capability.
         * \param f_ extended field whose elements are code symbols.
         * \param b_ first power of primitive element used to calculate generator polynomial.
         */
        EncoderRS(unsigned int t_, ExtendedField* f_, unsigned int b_ = 1);
        //! Function returns code generator polynomial.
        /*!
         * \return code generator polynomial.
         */
        Poly<FieldElement> getGenerator() const;
        //! Function returns first power of primitive element used to calculate generator polynomial.
        /*!
         * \return first power of primitive element used to calculate generator polynomial.
         */
        unsigned int getFirstPower() const;
        //! Function returns error correction capability of code.
        /*!
         * \return error correction capability.
         */
        unsigned int getErrCapability() const;
        //! Function returns extended field whose elements are code symbols.
        /*!
         * \return extended field whose elements are code symbols.
         */
        const ExtendedField* getExtendedField() const;
        //! Function encodes information polynomial with use of systematic code.
        /*!
         * \param m information polynomial.
         * \return encoded codeword.
         */
        Poly<FieldElement> encode_sys(const Poly<FieldElement>& m) const;
    };

}
#endif

