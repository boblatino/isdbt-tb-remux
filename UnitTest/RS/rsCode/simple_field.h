#ifndef SIMPLEFIELD_H
#define SIMPLEFIELD_H
#include <iostream>
#include <stdexcept>
#include "poly.h"
#include "field_element.h"

namespace errctrl {

    class FieldElement;

    //!  Galois Field class.

    /*!
     * Implementation of Galois field arithmetic.
     */

    class SimpleField {
    protected:
        //! Characteristic type.
        typedef unsigned int characteristic;
        //! Size type.
        typedef unsigned int size_type;
        //! Characteristic of Galois field. It must be a prime number.
        characteristic p;
        //! Function checks whether number is prime or not.
        /*!
         * \param n number to be checked.
         * \return true if number is prime, otherwise false.
         */
        bool is_prime(unsigned int n);
        //! Auxiliary function which checks whether polynomial is irreducible in given field.
        /*!
         * \param a polynomial to be checked.
         * \param temp auxiliary polynomial used in recursive calls.
         * \param pos auxiliary variable used in recursive calls.
         * \param f_dest field in which polynomial is tested.
         * \return true if polynomial is irreducible in given field, otherwise false.
         */
        static bool check_irreducible(const Poly<FieldElement>& a, Poly<FieldElement> temp, unsigned int pos, const SimpleField* f_dest);
    public:
        //! Constructor.
        /*!
         * \param p_ characteristic of field.
         */
        SimpleField(characteristic p_);
        //! Constructor.
        SimpleField();
        //! Destructor.
        virtual ~SimpleField();
        //! Function returns characteristic of field.
        /*!
         * \return characteristic of field.
         */
        characteristic getCharacteristic() const;
        //! Function returns size of field - number of elements in field.
        /*!
         * \return size of field.
         */
        virtual size_type size() const;
        //! Function adds two elements of field.
        /*!
         * \param a first summand.
         * \param b second summand.
         * \return sum of two elements.
         */
        virtual FieldElement::value_type add(const FieldElement& a, const FieldElement& b) const;
        //! Function multiplies two elements of field.
        /*!
         * \param a first factor.
         * \param b second factor.
         * \return product of two elements.
         */
        virtual FieldElement::value_type mul(const FieldElement& a, const FieldElement& b) const;
        //! Function multiplies element of field and integer.
        /*!
         * \param a element of field.
         * \param b an integer.
         * \return product of two elements.
         */
        virtual FieldElement::value_type mul(const FieldElement& a, int b) const;
        //! Function calculates additive inverse of element.
        /*!
         * \param a element whose additive inverse is returned.
         * \return additive inverse of input element.
         */
        virtual FieldElement::value_type a_inv(const FieldElement& a) const;
        //! Function calculates multiplicative inverse of element.
        /*!
         * \param a element whose multiplicative inverse is returned.
         * \return multiplicative inverse of input element.
         */
        virtual FieldElement::value_type m_inv(const FieldElement& a) const;

        //! Function checks whether polynomial is irreducible in given field.
        /*!
         * \param a polynomial to be checked.
         * \param f_dest field in which polynomial is tested.
         * \return true if polynomial is irreducible in given field, otherwise false.
         */
        static bool is_irreducible(const Poly<FieldElement>& a, const SimpleField* f_dest);
        //! Function checks whether polynomial is primitive in given field.
        /*!
         * \param a polynomial to be checked.
         * \param f_dest field in which polynomial is tested.
         * \return true if polynomial is primitive in given field, otherwise false.
         */
        static bool is_primitive(const Poly<FieldElement>& a, const SimpleField* f_dest);

        //!  Function prints element.
        /*!
         * \param s ostream object.
         * \param o element to be printed.
         * \return ostream object.
         */
        friend std::ostream & operator<<(std::ostream& s, SimpleField o);
    };

}
#endif