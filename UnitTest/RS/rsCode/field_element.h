#ifndef FIELDELEMENT_H
#define FIELDELEMENT_H



#include <iostream>
#include <cmath>

namespace errctrl {


    class SimpleField;

    //!  Galois field element class.

    /*!
     * Implementation of arithmetic of single Galois field elements.
     */
    class FieldElement {
    public:
        //!  Value type for Galois field element.

        typedef unsigned int value_type;
        //!  Constructor.
        /*!
         * \param val_ value of Galois field element.
         * \param f_ Galois field whose part is element.
         */
        FieldElement(value_type val_, SimpleField* f_);
        //!  Function returns value of element.
        /*!
         * \return value of element.
         */
        value_type getVal() const;
        //!  Function returns field whose part is element.
        /*!
         * \return field whose part is element.
         */
        const SimpleField* getField() const;
        //!  Function sets field whose part is element.
        /*!
         * \param f_new new field whose part is element.
         */
        void setField(SimpleField* f_new);
        //!  Function adds two elements of field.
        /*!
         * \param b one of summands.
         * \return sum of two elements.
         */
        FieldElement & operator+=(const FieldElement& b);
        //!  Function subtracts element from element.
        /*!
         * \param b subtrahend.
         * \return difference of two elements.
         */
        FieldElement & operator-=(const FieldElement& b);
        //!  Function multiplies element by element.
        /*!
         * \param b factor.
         * \return product of two elements.
         */
        FieldElement & operator*=(const FieldElement& b);
        //!  Function multiplies element by constant.
        /*!
         * \param b constant.
         * \return element multiplied by constant.
         */
        FieldElement & operator*=(const value_type& b);
        //!  Function divides element by element.
        /*!
         * \param b divisor.
         * \return quotient of division element by element.
         */
        FieldElement & operator/=(const FieldElement& b);
        //!  Function assigns new value of element.
        /*!
         * \param b new value of element.
         * \return element with new value.
         */
        FieldElement & operator=(const value_type& b);
        //!  Function calculates additive inverse of element.
        /*!
         * \return additive inverse of element.
         */
        FieldElement operator-() const;
        //!  Function prints element.
        /*!
         * \param s ostream object.
         * \param o element to be printed.
         * \return ostream object.
         */
        friend std::ostream & operator<<(std::ostream& s, FieldElement o);
    private:
        //!  Value of element.
        value_type val;
        //!  Galois field whose part is element.
        SimpleField* f;
    };
    //!  Function adds two elements of field.
    /*!
     * \param a first summand.
     * \param b second summand.
     * \return sum of two elements.
     */
    FieldElement operator+(const FieldElement& a, const FieldElement& b);
    //!  Function subtracts one element from second element.
    /*!
     * \param a minuend.
     * \param b subtrahend.
     * \return difference of two elements.
     */
    FieldElement operator-(const FieldElement& a, const FieldElement& b);
    //!  Function multiplies element by element.
    /*!
     * \param a first factor.
     * \param b second factor.
     * \return product of two elements.
     */
    FieldElement operator*(const FieldElement& a, const FieldElement& b);
    //!  Function multiplies element by constant.
    /*!
     * \param a constant.
     * \param b factor.
     * \return element multiplied by constant.
     */
    FieldElement operator*(const FieldElement::value_type& a, const FieldElement& b);
    //!  Function multiplies element by constant.
    /*!
     * \param a factor.
     * \param b constant.
     * \return element multiplied by constant.
     */
    FieldElement operator*(const FieldElement& a, const FieldElement::value_type& b);
    //!  Function divides element by constant.
    /*!
     * \param a dividend.
     * \param b divisor.
     * \return quotient of division of element by element.
     */
    FieldElement operator/(const FieldElement& a, const FieldElement& b);

    //!  Function compares two elements.
    /*!
     * \param a first element.
     * \param b second element.
     * \return true if elements are equal, otherwise false.
     */
    bool operator==(const FieldElement& a, const FieldElement& b);
    //!  Function compares value of element and value.
    /*!
     * \param a value.
     * \param b value of element.
     * \return true if values are equal, otherwise false.
     */
    bool operator==(const FieldElement::value_type& a, const FieldElement& b);
    //!  Function compares value of element and value.
    /*!
     * \param a value of element.
     * \param b value.
     * \return true if values are equal, otherwise false.
     */
    bool operator==(const FieldElement& a, const FieldElement::value_type& b);

    //!  Function compares two elements.
    /*!
     * \param a first element.
     * \param b second element.
     * \return false if elements are equal, otherwise true.
     */
    bool operator!=(const FieldElement& a, const FieldElement& b);
    //!  Function compares value of element and value.
    /*!
     * \param a value.
     * \param b value of element.
     * \return false if values are equal, otherwise true.
     */
    bool operator!=(const FieldElement::value_type& a, const FieldElement& b);
    //!  Function compares value of element and value.
    /*!
     * \param a value of element.
     * \param b value.
     * \return false if values are equal, otherwise true.
     */
    bool operator!=(const FieldElement& a, const FieldElement::value_type& b);

    //!  Function calculates power of Galois field element.
    /*!
     * \param a element.
     * \param b exponent.
     * \return power of element.
     */
    FieldElement pow(const FieldElement& a, int value);

}
#endif