#include "decoder_rs.h"

namespace errctrl {

    Poly<FieldElement> DecoderRS::calculate_err_eval_poly(const Poly<FieldElement>& syndrome_poly,
            const Poly<FieldElement>& err_locator) {
        Poly<FieldElement> syndromes(syndrome_poly);
        syndromes[0] = 0;
        Poly<FieldElement> result = err_locator;
        result = result * syndromes;
        FieldElement one(1, f);
        Poly<FieldElement> div_poly(&one, 0);
        div_poly = div_poly >> (2 * t + 1);
        result = result % div_poly;
        result[0] = 0;
        return result;
    }

    DecoderRS::DecoderRS(unsigned int t_, ExtendedField* f_, unsigned int b_) {
        f = f_;
        t = t_;
        b = b_;
    }

    Poly<FieldElement> DecoderRS::compute_syndrome_poly(const Poly<FieldElement>& r) {
        std::vector<FieldElement> syndrome_temp;
        for (unsigned int i = 0; i < 2 * t; i++) {
            unsigned char val = (1 + b + i) % (f->size());
            syndrome_temp.push_back(r.evaluate(FieldElement(val, f)));
        }
        Poly<FieldElement> syndrome_poly(syndrome_temp.data(), syndrome_temp.size() - 1);
        syndrome_poly = syndrome_poly >> 1;
        syndrome_poly[0] = 1;
        return syndrome_poly;
    }

    Poly<FieldElement> DecoderRS::bma(const Poly<FieldElement>& syndrome_poly) {
        unsigned int cnt = 2 * t;
        unsigned int L = 0;
        FieldElement one(1, f);
        Poly<FieldElement> err_locator(&one, 0);
        Poly<FieldElement> B = err_locator;
        for (unsigned int r = 1; r <= cnt; r++) {
            FieldElement delta(0, f);
            for (unsigned int j = 0; j <= L; j++) {
                delta = delta + syndrome_poly[r - j] * err_locator[j];
            }
            Poly<FieldElement> temp = err_locator;
            err_locator = err_locator - delta * (B >> 1);
            if (delta != 0 && (2 * L <= r - 1)) {
                L = r - L;
                B = temp / delta;
            } else {
                B = B >> 1;
            }
        }
        return err_locator;
    }

    std::vector<FieldElement> DecoderRS::chien_search(const Poly<FieldElement>& err_locator) {
        std::vector<FieldElement> err_loc_numbers;
        for (unsigned int i = 1; i < f->size(); i++) {
            if (err_locator.evaluate(FieldElement(i, f)) == (FieldElement::value_type)0) {
                err_loc_numbers.push_back(pow(FieldElement(i, f), -1));
            }
        }
        return err_loc_numbers;
    }

    std::vector<std::pair<FieldElement, FieldElement> >
    DecoderRS::forney(const std::vector<FieldElement>& err_loc_numbers, const Poly<FieldElement>& err_eval_poly,
            const Poly<FieldElement>& err_locator_der) {
        std::vector<std::pair<FieldElement, FieldElement> > result;
        for (unsigned int i = 0; i < err_loc_numbers.size(); i++) {
            FieldElement temp = err_loc_numbers[i];
            FieldElement temp_inv = pow(temp, -1);
            temp = -pow(temp, 2 - b);
            temp = temp * (err_eval_poly.evaluate(temp_inv) / err_locator_der.evaluate(temp_inv));
            std::pair<FieldElement, FieldElement> result_temp(err_loc_numbers[i], temp);
            result.push_back(result_temp);
        }
        return result;
    }

    Poly<FieldElement> DecoderRS::correct(const Poly<FieldElement>& r,
            const std::vector<std::pair<FieldElement, FieldElement> >& err_values) {
        Poly<FieldElement> c = r;
        for (unsigned int i = 0; i < err_values.size(); i++) {
            c[(err_values[i]).first.getVal() - 1] = c[(err_values[i]).first.getVal() - 1] - (err_values[i].second);
        }
        return c;
    }

    Poly<FieldElement> DecoderRS::decode_bma_sys(const Poly<FieldElement>& r) {
        Poly<FieldElement> received(r);
        Poly<FieldElement> syndrome_poly = compute_syndrome_poly(received);
        if (syndrome_poly.degree() == 0) {
            received = received << 2 * t;
            return received;
        }
        Poly<FieldElement> err_locator = bma(syndrome_poly);
        if (err_locator.degree() > t) {
            throw UnCorrectableException("Uncorrectable error pattern");
        }
        std::vector<FieldElement> err_loc_numbers = chien_search(err_locator);
        if (err_loc_numbers.size() == 0) {
            throw UnCorrectableException("Uncorrectable error pattern");
        }
        Poly<FieldElement> err_eval_poly = calculate_err_eval_poly(syndrome_poly, err_locator);
        Poly<FieldElement> err_locator_der = Poly<FieldElement>::derivative(err_locator);
        std::vector<std::pair<FieldElement, FieldElement> > err_values = forney(
                err_loc_numbers, err_eval_poly, err_locator_der);
        received = correct(received, err_values);
        received = received << 2 * t;
        return received;
    }

}