#ifndef DECODER_RS_H
#define	DECODER_RS_H

#include "field_element.h"
#include "extended_field.h"
#include "poly.h"
#include <vector>
#include <utility>

namespace errctrl {


    //!  UnCorrectableException class.

    /*!
     * Exception used to signal uncorrectable error patterns.
     */
    struct UnCorrectableException : public std::invalid_argument {

        UnCorrectableException(std::string const& message)
        : std::invalid_argument(message) {
        }
    };

    //!  Reed-Solomon decoder class.

    class DecoderRS {
    private:
        //! First power of primitive element used to calculate generator polynomial.
        unsigned int b;
        //! Error correction capability.
        unsigned int t;
        //! Extended field whose elements are code symbols.
        ExtendedField* f;
        //! Function calculates syndromes.
        /*!
         * \param r received word which is used to compute syndromes.
         * \return syndrome polynomial.
         */
        Poly<FieldElement> compute_syndrome_poly(const Poly<FieldElement>& r);
        //! Implementation of Berlekamp-Massey algorithm which calculates error locator polynomial.
        /*!
         * \param syndrome_poly syndrome polynomial.
         * \return error locator polynomial.
         */
        Poly<FieldElement> bma(const Poly<FieldElement>& syndrome_poly);
        //! Implementation of Chien search which calculates error positions.
        /*!
         * \param err_locator error locator polynomial.
         * \return error positions.
         */
        std::vector<FieldElement> chien_search(const Poly<FieldElement>& err_locator);
        //! Function calculates error evaulator polynomial.
        /*!
         * \param syndrome_poly syndrome polynomial.
         * \param err_locator error locator polynomial.
         * \return error evaluator polynomial.
         */
        Poly<FieldElement> calculate_err_eval_poly(const Poly<FieldElement>& syndrome_poly,
                const Poly<FieldElement>& err_locator);
        //! Implementation of Forney algorithm which calculates error values.
        /*!
         * \param err_loc_numbers error location numbers - positions of erroneous elements.
         * \param err_eval_poly error evaluator polynomial.
         * \param err_locator_der derivative of error locator polynomial.
         * \return polynomial of pairs of type (error position, error value).
         */
        std::vector<std::pair<FieldElement, FieldElement> >
        forney(const std::vector<FieldElement>& err_loc_numbers, const Poly<FieldElement>& err_eval_poly,
                const Poly<FieldElement>& err_locator_der);
        //! Function corrects received vector with use of previously calculated error positions and error values.
        /*!
         * \param r received word to be corrected.
         * \param err_values polynomial of pairs of type (error position, error value).
         * \return corrected received word.
         */
        Poly<FieldElement> correct(const Poly<FieldElement>& r,
                const std::vector<std::pair<FieldElement, FieldElement> >& err_values);
    public:
        //! Constructor.
        /*!
         * \param t_ error correction capability.
         * \param f_ extended field whose elements are code symbols.
         * \param b_ first power of primitive element used to calculate generator polynomial.
         */
        DecoderRS(unsigned int t_, ExtendedField* f_, unsigned int b_ = 1);

        //! Function decodes received word with use of Berlekamp-Massey algorithm.
        /*!
         * \param r received word.
         * \return corrected received word.
         */
        Poly<FieldElement> decode_bma_sys(const Poly<FieldElement>& r);
    };

}
#endif

