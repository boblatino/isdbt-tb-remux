#include "simple_field.h"
#include "field_element.h"

namespace errctrl {

    SimpleField::SimpleField() {
    }

    SimpleField::SimpleField(characteristic p_) : p(p_) {
        if (p == 1) {
            throw std::logic_error("Field characteristic cannot be 1");
        }
        if (p == 0) {
            throw std::logic_error("Field characteristic cannot be 0");
        }
        if (is_prime(p) == false) {
            throw std::logic_error("Field characteristic must be a prime number");
        }
    }

    SimpleField::~SimpleField() {
    }

    SimpleField::characteristic SimpleField::getCharacteristic() const {
        return p;
    }

    SimpleField::size_type SimpleField::size() const {
        return p;
    }

    FieldElement::value_type SimpleField::add(const FieldElement& a, const FieldElement& b) const {
        return (a.getVal() + b.getVal()) % p;
    }

    FieldElement::value_type SimpleField::mul(const FieldElement& a, const FieldElement& b) const {
        return (a.getVal() * b.getVal()) % p;
    }

    FieldElement::value_type SimpleField::mul(const FieldElement& a, int b) const {
        return (a.getVal() * b) % p;
    }

    FieldElement::value_type SimpleField::a_inv(const FieldElement& a) const {
        unsigned char result = (p - a.getVal()) % p;
        return result;
    }

    FieldElement::value_type SimpleField::m_inv(const FieldElement& a) const {
        FieldElement temp = a;
        unsigned char result = pow(temp, p - 2).getVal();
        return result;
    }

    std::ostream & operator<<(std::ostream& s, SimpleField o) {
        const int p = o.getCharacteristic();
        for (int i = 0; i < p; i++) {
            s << i << ' ';
        }
        return s;
    }

    bool SimpleField::is_prime(unsigned int n) {
        if (n == 0 || n == 1) {
            return false;
        }
        unsigned int n_sqrt = (unsigned int)std::sqrt((double)n);
        for (unsigned int i = 2; i <= n_sqrt; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    bool SimpleField::is_irreducible(const Poly<FieldElement>& a, const SimpleField* f_dest) {
        if (a.degree() < 2) {
            return true;
        }
        Poly<FieldElement> a_temp(a);
        for (unsigned int i = 0; i < a_temp.size(); i++) {
            a_temp[i].setField(const_cast<SimpleField*> (f_dest));
        }
        std::vector<FieldElement> elems;
        for (unsigned int i = 0; i < a_temp.degree(); i++) {
            elems.push_back(FieldElement(0, const_cast<SimpleField*> (f_dest)));
        }
        Poly<FieldElement> temp(elems.data(), elems.size() - 1);
        for (unsigned int i = 0; i < f_dest->size(); i++) {
            if (check_irreducible(a_temp, temp, 1, const_cast<SimpleField*> (f_dest)) == false) {
                return false;
            }
            temp[0] = FieldElement(i + 1, const_cast<SimpleField*> (f_dest));
        }
        return true;
    }

    bool SimpleField::check_irreducible(const Poly<FieldElement>& a, Poly<FieldElement> temp, unsigned int pos, const SimpleField* f_dest) {
        if (pos == a.degree() + 1) {
            if (temp.degree() == 0) {
                return true;
            } else {
                Poly<FieldElement> modulo = a % temp;
                if (modulo.degree() == 0 && modulo[0] == 0) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            for (unsigned int i = 0; i < f_dest->size(); i++) {
                if (check_irreducible(a, temp, pos + 1, const_cast<SimpleField*> (f_dest)) == false) {
                    return false;
                }
                temp[pos - 1] = FieldElement(i + 1, const_cast<SimpleField*> (f_dest));
            }
        }
        return true;
    }

    bool SimpleField::is_primitive(const Poly<FieldElement>& a, const SimpleField* f_dest) {
        Poly<FieldElement> a_temp(a);
        for (unsigned int i = 0; i < a_temp.size(); i++) {
            a_temp[i].setField(const_cast<SimpleField*> (f_dest));
        }
        if (is_irreducible(a_temp, f_dest) == false) {
            return false;
        }
        unsigned int deg = (unsigned int)std::pow((double)f_dest->getCharacteristic(), (int)a_temp.degree());
        std::vector<FieldElement> temp;
        for (unsigned int i = 0; i < a_temp.degree(); i++) {
            temp.push_back(FieldElement(0, const_cast<SimpleField*> (f_dest)));
        }
        temp.push_back(FieldElement(1, const_cast<SimpleField*> (f_dest)));

        Poly<FieldElement> temp_poly(temp.data(), a_temp.degree());
        for (unsigned int i = a_temp.degree() - 1; i <= deg - 2; i++) {
            Poly<FieldElement> xn1 = temp_poly;
            xn1[0] = -FieldElement(1, const_cast<SimpleField*> (f_dest));
            Poly<FieldElement> result = xn1 % a_temp;

            if (result.degree() == 0 && result[0] == 0) {
                if (i < deg - 2) {
                    return false;
                } else if (i == deg - 2) {
                    return true;
                }
            }
            temp_poly = temp_poly >> 1;
        }
        return false;
    }

}


