#include "Rs/rsCode/simple_field.h"
#include "Rs/rsCode/field_element.h"
#include "Rs/rsCode/extended_field.h"
#include "Rs/rsCode/poly.h"
#include "Rs/rsCode/encoder_rs.h"

/*
 1x^0 + 0x^1 + 1x^2 + 1x^3 + 1x^4 + 0x^5 + 0x^6 + 0x^7 + 1x^8 
 const unsigned int primitive_polynomial05[]    = {1, 0, 1, 1, 1, 0, 0, 0, 1};
const unsigned int primitive_polynomial_size05 = 9;
*/

void testReedSolomon() 
{
	// field GF (2)
	errctrl :: SimpleField field_2 (28) ;
	// primitive polynomial x^3+ x+1 for GF (8)
	errctrl :: FieldElement elems_2 [9] = {
		errctrl :: FieldElement (1, & field_2 ),
		errctrl :: FieldElement (0, & field_2 ),
		errctrl :: FieldElement (1, & field_2 ),
		errctrl :: FieldElement (1, & field_2 ),
		errctrl :: FieldElement (1, & field_2 ),
		errctrl :: FieldElement (0, & field_2 ),
		errctrl :: FieldElement (0, & field_2 ),
		errctrl :: FieldElement (0, & field_2 ),
		errctrl :: FieldElement (1, & field_2 )
	};
	errctrl :: Poly < errctrl :: FieldElement >
	generator ( elems_2, 8);
	// field GF (8)
	errctrl :: ExtendedField gf8 (2, 3, generator );
	// encoder for RS (7 ,3) code
	errctrl :: EncoderRS e(2, & gf8 );
	// information polynomial x^2+ alpha ^2 x+ alpha
	errctrl :: FieldElement m [3] = {
		errctrl :: FieldElement (2, & gf8 ),
		errctrl :: FieldElement (3, & gf8 ),
		errctrl :: FieldElement (1, & gf8 )
	};
	errctrl :: Poly < errctrl :: FieldElement > m_poly (m, 2);

	// encoding
	errctrl :: Poly < errctrl :: FieldElement > c =
		e. encode_sys ( m_poly );
}