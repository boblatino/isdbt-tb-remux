#include <fstream>
#include <assert.h>
#include <iostream>

//#define UNTIL_IIP_COUNT 4598  // 4599 - 1 in first file
//#define FILENAME "train.txt"

#define UNTIL_IIP_COUNT 5108
#define FILENAME "canal9.txt"

void pattern() 
{
	std::ifstream file(FILENAME, std::ios::in);
	assert(file.good());

	char* buffer = new char[400000];
	file.read(buffer, 400000);
	size_t read = static_cast<size_t>(file.gcount());

	char* p = buffer;

	int* a = new int[UNTIL_IIP_COUNT];
	int* b = new int[UNTIL_IIP_COUNT];
	int* c = new int[UNTIL_IIP_COUNT];
	int* n = new int[UNTIL_IIP_COUNT];
	memset(a, 0, UNTIL_IIP_COUNT * sizeof(int));
	memset(b, 0, UNTIL_IIP_COUNT * sizeof(int));
	memset(c, 0, UNTIL_IIP_COUNT * sizeof(int));
	memset(n, 0, UNTIL_IIP_COUNT * sizeof(int));

	// get the I count on the file
	int icount = 0;
	char* ibuff = buffer;
	for(size_t j = 0; j < read; ++j) {
		if(*ibuff == 'I') {
			icount++;
		}
		ibuff++;
	}

	for(size_t j = 0; j <icount; ++j) {
		int i = 0;
		while(*p != 'I') {
			if(*p == 'A') {
				a[i]++;
			} else if(*p == 'B') {
				b[i]++;
			} else if(*p == 'C') {
				c[i]++;
			} else if(*p == 'N') {
				n[i]++;
			}
			p++;
			++i;
		}
		p++;
	}
	

	// Count a, b, c and n per IIP
	int* aiip = new int[icount];
	int* biip = new int[icount];
	int* ciip = new int[icount];
	int* niip = new int[icount];
	memset(aiip, 0, icount * sizeof(int));
	memset(biip, 0, icount * sizeof(int));
	memset(ciip, 0, icount * sizeof(int));
	memset(niip, 0, icount * sizeof(int));

	p = buffer;
	for(size_t j = 0; j <icount; ++j) {
		while(*p != 'I') {
			if(*p == 'A') {
				aiip[j]++;
			} else if(*p == 'B') {
				biip[j]++;
			} else if(*p == 'C') {
				ciip[j]++;
			} else if(*p == 'N') {
				niip[j]++;
			}
			p++;
		}
		p++;
	}

	std::ofstream out("out.txt");

	for(size_t i = 0; i < UNTIL_IIP_COUNT; ++i) {
		/*assert(a[i] == 1 || a[i] == 2);
		assert(b[i] == 1 || b[i] == 2);
		assert(c[i] == 1);
		assert(n[i] == 1);*/

		out << " a " << a[i] << " b " << b[i] << " c " << c[i] << " n " << n[i] <<  " i " << i << std::endl;
		/*if(a[i] > 0) {
			assert(b[i] == 0 && c[i] == 0);
		}

		if(b[i] > 0) {
			assert(a[i] == 0 && c[i] == 0);
		}

		if(c[i] > 0) {
			assert(a[i] == 0 && b[i] == 0);
		}*/
		
	}

	out << "  --  Cantidad de tipo de dato por IIP --- " << std::endl;

	for(size_t j = 0; j <icount; ++j) {
		out << " a " << aiip[j] << " b " << biip[j] << " c " << ciip[j] << " n " << niip[j] <<  " NUMERO DE PAQUETE IIP " << j << " SUMA " <<  aiip[j] + biip[j] + ciip[j] + niip[j] << std::endl;
	}

}
