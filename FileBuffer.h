#pragma once
#include <fstream>
#include <assert.h>

// Max buffer 500 MB
#define MAX_BUFFER_SIZE 500 * 1024 * 1024

class FileBuffer : private std::fstream 
{
public:
	FileBuffer(const char * filename, ios_base::openmode mode);
	size_t read(unsigned char* buffer, size_t size);
	~FileBuffer();

	bool is_open() const;
	bool eof() const;
	void seekg (streamoff off);
	void write( unsigned char* buffer, size_t size );
	void flushWrite();

private: 
	FileBuffer(const FileBuffer&);
	FileBuffer& operator=(const FileBuffer&);

	size_t fileSize();
	void initializeWrite();
	void initializeRead();

	unsigned char* mBuffer;
	size_t mMaxCount;
	size_t mCurrentCount;
	size_t mFileSize;
};