#include "TSdecode.h"





TSdecode::TSdecode(const string& fileName) : tsFile(fileName, ios::binary)

{	

}

	

TSdecode::~TSdecode()

{

	tsFile.close();

}



ifstream& TSdecode::getRawFile()

{

	return tsFile;

}

		

void TSdecode::startDecode()

{

	if (tsFile.is_open())

	{

		streampos syncBytePos = searchForSyncByte(0);

		tsFile.seekg(syncBytePos);

		const streampos fileS = fileSize();

		headers.reserve(static_cast<unsigned int>(fileS) / TS_PACKET_SIZE);

		TS_header header;

		try 

		{

			while(!tsFile.eof())

			{

				TSheaderDecode(header);

				headers.push_back(header); 

				syncBytePos += TS_PACKET_SIZE;

				searchForSyncByte(syncBytePos);

			}

		} catch(const NotfoundException&) 

		{

		}

	}

	else 

	{

		throw NotfoundException();

	}

}



streampos TSdecode::fileSize() 

{

	tsFile.seekg(0, std::ios::end);

	const streampos size = tsFile.tellg();

	tsFile.seekg(0);

	return size;

}





void TSdecode::displayTransportHeaders()

{	

	#ifdef PRINT_TS

	for(unsigned int i =0; i<PRINT_TS; ++i)

	{

		printf("\nSync byte:     \t \t x%x",headers[i].sync_byte);

		printf("\nError indicator:  \t d%d",headers[i].transport_error_indicator);

		printf("\nPayload start: \t \t d%d",headers[i].payload_start_indicator);

		if (headers[i].payload_start_indicator == 0x1)

			printf("\t Payload is start of PID %d", headers[i].PID);

		printf("\nTans priority: \t \t d%d",headers[i].transport_priority);

		printf("\nPID:           \t \t x%x \t d%d",headers[i].PID, headers[i].PID);

		printf("\nScrambling:    \t \t d%d",headers[i].transport_scrambling_control);

		if (headers[i].transport_scrambling_control == 0x0)

			printf("\t not scrambled");

		printf("\nAdaption:      \t \t d%d",headers[i].adaption_field_control);

		if (headers[i].adaption_field_control == 0x01)

		{	

			printf("\t no adaption field, payload only");

		}

		if (headers[i].adaption_field_control == 0x02)

		{	

			printf("\t adaption field only, no payload");

		}

		if (headers[i].adaption_field_control == 0x03)

		{	

			printf("\t adaption field followed by payload");

		}

		printf("\nContinuity:    \t \t d%d",headers[i].continuity_counter);

	}

	#endif

}



streampos TSdecode::searchForSyncByte(const streampos& offset)

{	

	//Set the file to the appropiate offset

	tsFile.seekg(offset);

	char sync_data = 0;

	while (!tsFile.eof())

	{

		tsFile.read(&sync_data, 1);

		if(sync_data == 0x47)

		{

			tsFile.unget();

			return tsFile.tellg();

		}

	}

	//Not found here

	throw NotfoundException();

}



void TSdecode::TSheaderDecode(TS_header& header)

{

	unsigned char tsRawHeader[4];

	tsFile.read((char*)&tsRawHeader, sizeof(tsRawHeader));	

	if(!tsFile.eof())

	{

		// use bit masking and shifting to get the data we want from header

		header.sync_byte = tsRawHeader[0];

		header.transport_error_indicator = (tsRawHeader[1] & 0x80) >> 7;

		header.payload_start_indicator = (tsRawHeader[1] & 0x40) >> 6;

		header.transport_priority = (tsRawHeader[1] & 0x20) >> 5;

		header.PID = ((tsRawHeader[1] & 31) << 8) | tsRawHeader[2];

		header.transport_scrambling_control = (tsRawHeader[3] & 0xC0);

		header.adaption_field_control = (tsRawHeader[3] & 0x30) >> 4;

		header.continuity_counter = (tsRawHeader[3] & 0xF);

	}

}	



unsigned int TSdecode::getPid(size_t index)

{

	return headers[index].PID;

}