#pragma once

#define CRC32POLY       0x04c11db7
unsigned int calculateCRC(unsigned char* aBuffer, size_t size);