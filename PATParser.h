#pragma once
#include "Conmon.h"
#include <map>
#include <assert.h>

struct PAT_header {
	unsigned char table_id;
	unsigned int section_length;
	unsigned char current_next_indicator;
	unsigned char section_number;
	unsigned char last_section_number;
	unsigned int program_number; //N of this 
	unsigned int program_map_PID;
};

class PATParser {
public:
	PATParser() : mIsFilled(false) {}
	void read(unsigned char* aBuffer) {
		PAT_header header;
		header.table_id = aBuffer[0];
		assert(header.table_id == 0x0);
		header.section_length = ((aBuffer[1] & 0xF) << 8) | aBuffer[2];
		header.current_next_indicator = aBuffer[5] & 0x01;
		header.section_number = aBuffer[6];
		header.last_section_number = aBuffer[7];

		if (header.current_next_indicator != 0x1) {
			return;
		}

		const size_t ndx = 8; // starts in the buffer[8]
		// The length is counted from below it so discard 9 (bytes before it) and calculate the program count
		const size_t programCount = (header.section_length - 9) / 4; // this count should be exact
		size_t count = 0;
		for (size_t i = 0; i < programCount; ++i)
		{
			header.program_number = (aBuffer[ndx+count++] << 8);
			header.program_number |= aBuffer[ndx+count++];
			header.program_map_PID = ((aBuffer[ndx+count++] & 0x1f) << 8);
			header.program_map_PID |= aBuffer[ndx+count++];

			// Skip the Network Information Table (NIT).
			if (header.program_number != 0x0)
			{
				// Store the PTM PID if we haven't already.
				mProgramNumberToMapPID[header.program_map_PID] = header.program_number;
			}
		}

		// If this is the last section number, we're done.
		assert(header.section_number == header.last_section_number);
		mIsFilled = true;
	}

	bool isFilled() { return mIsFilled; }
	
	// program pap pid KEY, program number VALUE
	std::map<unsigned int, unsigned int> mProgramNumberToMapPID;
	bool mIsFilled;
};