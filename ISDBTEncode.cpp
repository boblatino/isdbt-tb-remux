#include "ISDBTEncode.h"

#include <bitset>

#include "TSdecode.h"



using namespace std;



ISDBTEncode::ISDBTEncode(TSdecode& tsDecode, const string& btsFileName, const string& configFileName) 

											   : tsDecode(tsDecode), 

	                                           btsFile(btsFileName, ios::binary), 

											   config(configFileName)

{

	if(btsFile.is_open())

	{

		istream& tsFile = tsDecode.getRawFile();

		tsFile.clear();

		tsFile.seekg(0, ios_base::beg);			



		//add TS part and then isdbt part

		const streampos fileSize = tsDecode.fileSize();

		long packetNumber = 0;

		while(!tsFile.eof() && (fileSize - tsFile.tellg()) >= TS_PACKET_SIZE)

		{			

			char buffer [TS_PACKET_SIZE]; 

			tsFile.read(buffer, sizeof(buffer));

			btsFile.write(buffer, sizeof(buffer));

			const bitset<8*8>& isdbtInfo = createISDBTInfo(tsDecode.getPid(packetNumber));

			unsigned long long info = isdbtInfo.to_ullong();

			btsFile.write((char*)&info, sizeof(info));

			packetNumber++;

		}



		btsFile.close();

		

	}

}



bitset<8*8> ISDBTEncode::createISDBTInfo(unsigned int pid)

{

	bitset<8*8> info;

	size_t i=0;



	//Byte 0		

	//TMCC ID Televisi�n digital terrestre 10

	info[7+i] = true;

	info[6+i] = false;



	//Reservado siempre en 1

	info[5+i] = true;



	//buffer_reset_control_flag normalmente 0

	info[4+i] = false;



	//switch-on_control_flag_for_emergency broadcasting normalmente 0

	info[3+i] = false;



	//initialization_timing_head_ packet_flag es uno cuando la cuenta llega a 1111

	const bitset<4>& count = getCountDownIndex();

	info[2+i] = count.to_ulong() == 0x10; 



	//frame_head_packet_flag 

	/* Discrimina la cabecera de cuadros m�ltiples. La cabecera del 

	paquete de cuadros m�ltiples es �1� a pesar del n�mero par o 

	n�mero impar de cuadros; los otros son �0�. */

	info[1+i] = true; 



	//frame_indicator 

	/*

	Durante el cuadro par (W0) del cuadro OFDM , es �0� 

	Durante el cuadro impar (W1) del cuadro OFDM, es �1� 		

	*/

	static bool evenFrame = false;

	info[0+i] = evenFrame;

	evenFrame = !evenFrame;



	//Byte 1

	i+=8;



	//layer_indicator  

	/*

	Indica la jerarqu�a por la cual el TSP es transmitido  

	�0000� un TSP nulo que no es transmitido por ninguna de las 

	jerarqu�as A, B o C. 

	�0001�: TSP transmitida por la jerarqu�a A 

	�0010�: TSP transmitida por la jerarqu�a B 

	�0011�: TSP transmitida por la jerarqu�a C 

	0100: TSP que transmite datos AC, pero no es transmitido por 

	ninguna jerarqu�a A, B o C. 

	�0101� - �0111�: TSP que provee servicios de multiplexaci�n de los 

	datos originales 

	�1000�: TSP que transmite IIP,  pero no transmite ninguna de las 

	jerarqu�as A, B o C 

	�1001� -  1111�: TSP que provee servicio de multiplexaci�n de los 

	datos originales 		

	*/

	const bitset<4>& hierarchy = getHierarchy(pid);

	info[7+i] = hierarchy[0];

	info[6+i] = hierarchy[1];

	info[5+i] = hierarchy[2];

	info[4+i] = hierarchy[3];



	//count_down_index 

	/*

	�ndice de conmutaci�n de par�metros de transmisi�n descrito en la informaci�n TMCC 		

	*/

	info[3+i] = count[0]; //REVISAR CON DEBUG

	info[2+i] = count[1];

	info[1+i] = count[2];	

	info[0+i] = count[3];



	//Byte 2

	i+=8;



	//AC_data_invalid_flag   

	/*

	Cuando AC data no se agrega al byte fantasma: �1� 

	Cuando datos AC se agregan al dummy byte: �0� 

	*/

	info[7+i] = true;

		

	//AC_data_effective_bytes 

	/*

	�00�: 1-byte 

	�01�: 2-byte 

	�10�: 3-byte 

	�11�: 4-byte (incluyendo caso en que los datos AC no se agregan 

	al dummy byte) 

	Entre los bytes 4 a 7, la posici�n del byte a ser usado se

	especificar� por cada proveedor de servicio		

	*/

		

	info[6+i] = false;

	info[5+i] = false;



	//TSP_counter parte 1

	/*

	Un contador en el cual la cabecera del cuadro multiplex es 0 e 

	incrementado de uno en uno en el orden de los paquetes. El 

	incremento incluye NULL-TSP, TSP que transmite IIP o datos AC 

	etc.		

	*/	

	const bitset<13> packetCount = getPacketCount();

	info[4+i] = packetCount[0];

	info[3+i] = packetCount[1]; 

	info[2+i] = packetCount[2];

	info[1+i] = packetCount[3];

	info[0+i] = packetCount[4];



	//Byte 3

	i+=8;

	//TSP_counter parte 2

	info[7+i] = packetCount[5];

	info[6+i] = packetCount[6];

	info[5+i] = packetCount[7];

	info[4+i] = packetCount[8];

	info[3+i] = packetCount[9];

	info[2+i] = packetCount[10];

	info[1+i] = packetCount[11];

	info[0+i] = packetCount[12];



	//Los datos de AC van en 0 en los bytes 4,5,6,7



	return info;

}



bitset<4> ISDBTEncode::getCountDownIndex()

{

	static int count = 0x10;

	bitset<4> countBits(count);

	if(count != 0) 

	{

		count--;

	}

	else

	{

		count = 0x10;

	}	

	return countBits;

}



bitset<4> ISDBTEncode::getHierarchy(unsigned int pid)

{

	static const char* nullPacketID = "0000";

	static const char* hierAPacketID = "0001";

	static const char* hierBPacketID = "0010";

	static const char* hierCPacketID = "0011";



	switch(config.getHierarchy(pid))

	{

		case ConfigParser::A:

			return bitset<4>(hierAPacketID);

		case ConfigParser::B:

			return bitset<4>(hierBPacketID);

		case ConfigParser::C:

			return bitset<4>(hierCPacketID);

		default:

			return bitset<4>(nullPacketID);	

		

	}

}



bitset<13> ISDBTEncode::getPacketCount()

{

	static int count = 0;

	bitset<13> countBits(count);

	if(count != 0x2000) 

	{

		count++;

	}

	else

	{

		count = 0;

	}	

	return countBits;

}

