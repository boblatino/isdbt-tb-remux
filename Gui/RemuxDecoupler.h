#pragma once
#include "IRemux.h"

class RemuxDecoupler : public IRemux  {
public:

virtual void parseISDBTFile( ISDBTParseResult& result ) 
{

}

virtual void convertToTS( const std::string& writeFileName ) 
{

}

virtual void parseTs( std::set<ProgramInformation>& programs ) 
{

}

virtual void convertToBTS( const GuiInfo& guiInfo, const std::string& writeFileName ) 
{

}

};