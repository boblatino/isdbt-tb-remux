#ifndef GUI_H
#define GUI_H

#include <QtGui/QMainWindow>
#include <QtGui/QFileDialog>
#include "ui_gui.h"
#include "configuration.h" 
#include <memory>
#include "../RemuxDecoupleThread.h"
#include "../Remux.h"
#include "RemuxNotifier.h"

class Gui : public QMainWindow, Ui::MainGui
{
	Q_OBJECT

public:
	Gui(QWidget *parent = 0, Qt::WFlags flags = 0);
	~Gui();

private slots:
	void openConfigWindow();
	void openFileWindow();
	void openFile(const QString &);
	void convertToTs();
	void configReady(const GuiInfo&);
	void convertToBTS();

	void fillISDBTParseResult(const ISDBTParseResult&);
	void convertToBtsFinished();
	void convertToTsFinished();
	void showAbout();

private:
	QFileDialog* mFileDialog;
	std::auto_ptr<RemuxDecoupleThread> mRemux;
	std::auto_ptr<Remux> mRemuxBase;
	GuiInfo mGuiInfo;
	RemuxNotifier mNotifier;
};

#endif // GUI_H
