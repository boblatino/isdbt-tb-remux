#pragma once
#include <QtCore/QObject>
#include "../IRemux.h"

struct Progress {
	size_t speed;
};

struct RemuxNotifier : public QObject {
Q_OBJECT
public:

void informProgress(const Progress& progress) {
	emit informProgressSignal(progress);
}

void parseISDBTFileResult(const ISDBTParseResult& result) {
	emit parseISDBTFileResultSignal(result);
}

void parseTsResult(const std::set<ProgramInformation>& programInfo) {
	emit parseTsResultSignal(programInfo);
}

void convertToBTSFinished() {
	emit convertToBTSFinishedSignal();
}

void convertToTSFinished() {
	emit convertToTSFinishedSignal();
}

signals:
	void informProgressSignal(const Progress&);
	void parseISDBTFileResultSignal(const ISDBTParseResult&);
	void parseTsResultSignal(const std::set<ProgramInformation>&);
	void convertToBTSFinishedSignal();
	void convertToTSFinishedSignal();

};