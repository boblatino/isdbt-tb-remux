#include "gui.h"

#include <QInputDialog>
 #include <QMessageBox>
#include "../LieToMe.h"



Gui::Gui(QWidget *parent, Qt::WFlags flags)

	: QMainWindow(parent, flags) 

{
	setupUi(this);
	qRegisterMetaType<ISDBTParseResult>("ISDBTParseResult");
	mFileDialog = new QFileDialog(this);
	mFileDialog->setFileMode(QFileDialog::AnyFile);
	mFileDialog->setNameFilter(tr("TS (*.ts *.trp);;BTS (*.bts);;Todos (*.*)"));		

	connect(actionExit, SIGNAL(triggered()), this, SLOT(close()));
	connect(actionConfigure, SIGNAL(triggered()), this, SLOT(openConfigWindow()));
	connect(actionOpen, SIGNAL(triggered()), this, SLOT(openFileWindow()));
	connect(mFileDialog, SIGNAL(fileSelected(const QString &)), this, SLOT(openFile(const QString &)));
	connect(actionBTS_to_TS, SIGNAL(triggered()), this, SLOT(convertToTs()));
	connect(actionTS_to_BTS, SIGNAL(triggered()), this, SLOT(convertToBTS()));
	connect(&mNotifier, SIGNAL(parseISDBTFileResultSignal(const ISDBTParseResult&)), this, SLOT(fillISDBTParseResult(const ISDBTParseResult&)));	
	connect(&mNotifier, SIGNAL(convertToBTSFinishedSignal()), this, SLOT(convertToBtsFinished()));	
	connect(&mNotifier, SIGNAL(convertToTSFinishedSignal()), this, SLOT(convertToTsFinished()));	
	connect(actionAbout, SIGNAL(triggered()), this, SLOT(showAbout()));

	packetInfoBox->setUndoRedoEnabled(false);	
	mRemuxBase.reset(new Remux());
	mRemux.reset(new RemuxDecoupleThread(*mRemuxBase, mNotifier));
	workingLabel->hide();
}

void Gui::showAbout()
{
	QMessageBox::information(this, tr("Remux"),
	tr("Remultiplexor para ISDBT norma Argentina\n por Ramiro del Corro para UBP\n Contacto: ramiro.del.corro@gmail.com"),
	QMessageBox::Ok);
}

void Gui::fillISDBTParseResult(const ISDBTParseResult& result) {
	LieToMe ltm;
	try {
		std::string out = ltm.getPrintout(result);
	
		packetInfoBox->setPlainText(QString::fromStdString(out));
		layerACountLabel->setText(QString::number(ltm.c.layerACount));
		layerBCountLabel->setText(QString::number(ltm.c.layerBCount));
		layerCCountLabel->setText(QString::number(ltm.c.layerCCount));
		FrameCountLabel->setText(QString::number(ltm.c.packetsPerframeCount));
		nullCountLabel->setText(QString::number(ltm.c.nullCount));
		workingLabel->hide();
	} catch(const std::exception&) {
		QMessageBox::information(this, tr("Remux"),
		tr("El archivo posee una configuracion invalida\n"),
		QMessageBox::Ok);

		packetInfoBox->setPlainText("");
		layerACountLabel->setText(QString::number(0));
		layerBCountLabel->setText(QString::number(0));
		layerCCountLabel->setText(QString::number(0));
		FrameCountLabel->setText(QString::number(0));
		nullCountLabel->setText(QString::number(0));
		workingLabel->hide();
	}
}

void Gui::convertToBtsFinished() 
{
	workingLabel->hide();
	QMessageBox::information(this, tr("Remux"),
	tr("El archivo BTS fue creado con exito!\n"),
	QMessageBox::Ok);
}

void Gui::convertToTsFinished()
{
	workingLabel->hide();
	QMessageBox::information(this, tr("Remux"),
	tr("El archivo TS fue creado con exito!\n"),
	QMessageBox::Ok);
}

Gui::~Gui()
{
}

void Gui::openConfigWindow()
{
	Configuration* configGui = new Configuration(mRemux.get(), this, mNotifier);
	connect(configGui, SIGNAL(configReadySignal(const GuiInfo&)), SLOT(configReady(const GuiInfo&)));
	configGui->exec();
}

void Gui::openFileWindow()
{
	mFileDialog->exec();
}

void Gui::convertToTs() 
{
	bool ok;
	QString readFileName = QString::fromStdString(mRemux->getFilename());
	QStringList tokens = readFileName.split('/');
	QString path = readFileName.remove(tokens.last());	

	QString text = QInputDialog::getText(this, tr("Nombre de archivo TS"),
		tr("Indique el nombre del archivo a crear"), QLineEdit::Normal, "", &ok);
	if (ok && !text.isEmpty()) {
		mRemux->convertToTS((path + text + ".ts").toStdString());	
	}	
}

void Gui::openFile( const QString & fileSelected )

{
	//Parse the file to get type and load up the info in the gui
	mRemuxBase->open(fileSelected.toStdString());
	FILE_TYPE type = mRemux->getFileType();
	if(type == BTS) {
		ISDBTParseResult result;
		workingLabel->show();
		mRemux->parseISDBTFile(result);
		actionBTS_to_TS->setEnabled(true);
	} else if(type == TS) {
		QMessageBox::information(this, tr("Remux"),
			tr("El archivo abierto es de tipo TS. Ahora puede configurar los parametros BTS.\n"),
			QMessageBox::Ok);
		actionConfigure->setEnabled(true);
	} else {
		QMessageBox::information(this, tr("Remux"),
			tr("El archivo no es valido.\n"),
			QMessageBox::Ok);
	}
}

void Gui::configReady( const GuiInfo& guiInfo)
{
	mGuiInfo = guiInfo;
	actionTS_to_BTS->setEnabled(true);
}

void Gui::convertToBTS()
{
	bool ok;
	QString readFileName = QString::fromStdString(mRemux->getFilename());
	QStringList tokens = readFileName.split('/');
	QString path = readFileName.remove(tokens.last());

	QString text = QInputDialog::getText(this, tr("Nombre de archivo BTS"),
		tr("Indique el nombre del archivo BTS a crear"), QLineEdit::Normal, "", &ok);

	if (ok && !text.isEmpty()) {
		mRemux->convertToBTS(mGuiInfo, (path + text + ".bts").toStdString());
		workingLabel->show();
	}	
}