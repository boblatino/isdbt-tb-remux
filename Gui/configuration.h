#pragma once

#include <QtGui/QDialog>
#include "ui_configuration.h"
#include "../Conmon.h"
#include "../IRemux.h"
#include "../GuiToBTS.h"
#include "RemuxNotifier.h"

// Add time interleaving box as:
/*
MODE1, 0,4,8,16
MODE2, 0,2,4,8
MODE3, 0,1,2,4
*/

class Configuration : public QDialog, Ui::Configuration
{
	Q_OBJECT

public:
	Configuration(IRemux* remux, QWidget *parent, RemuxNotifier& notifier, Qt::WFlags flags = 0) : QDialog(parent), mRemux(remux) {
		setupUi(this);
		qRegisterMetaType<ProgramInformation>("ProgramInformation");
		qRegisterMetaType<std::set<ProgramInformation> >("std::set<ProgramInformation>");

		connect(aButton, SIGNAL(released()), SLOT(buttonAPressed()));
		connect(bButton, SIGNAL(released()), SLOT(buttonBPressed()));
		connect(cButton, SIGNAL(released()), SLOT(buttonCPressed()));
		connect(resetButton, SIGNAL(released()), SLOT(buttonResetPressed()));	
		connect(acceptButton, SIGNAL(released()), SLOT(prepareConfig()));
		connect(acceptButton, SIGNAL(released()), SLOT(accept()));
		connect(cancelButton, SIGNAL(released()), SLOT(accept()));		

		connect(&notifier, SIGNAL(parseTsResultSignal(const std::set<ProgramInformation>&)), this, SLOT(fillTSParseResult(const std::set<ProgramInformation>&)));

		mRemux->parseTs(mPrograms);
	}

signals:
	void configReadySignal(const GuiInfo&);

private slots:
	void buttonAPressed() {
		QList<QListWidgetItem*> pidSelected = programList->selectedItems();
		if(pidSelected.empty() == false) {
			QListWidgetItem* item = programList->takeItem(programList->currentRow());
			layerAList->addItem(item);
		}
	}

	void buttonBPressed() {
		QList<QListWidgetItem*> pidSelected = programList->selectedItems();
		if(pidSelected.empty() == false) {
			QListWidgetItem* item = programList->takeItem(programList->currentRow());
			layerBList->addItem(item);
		}
	}

	void buttonCPressed() {
		QList<QListWidgetItem*> pidSelected = programList->selectedItems();
		if(pidSelected.empty() == false) {
			QListWidgetItem* item = programList->takeItem(programList->currentRow());
			layerCList->addItem(item);
		}
	}

	void buttonResetPressed() {

		while(layerAList->count() != 0) {
			QListWidgetItem* item = layerAList->takeItem(0);
			programList->addItem(item);
		}
		while(layerBList->count() != 0) {
			QListWidgetItem* item = layerBList->takeItem(0);
			programList->addItem(item);
		}
		while(layerCList->count() != 0) {
			QListWidgetItem* item = layerCList->takeItem(0);
			programList->addItem(item);
		}
	}

	void prepareConfig(){
		GuiInfo config;
		config.mode = modeCombo->currentIndex();
		config.guard = guardIntervalCombo->currentIndex();
		config.layerA.numberOfSegments = lASegmentsCombo->currentIndex();
		config.layerB.numberOfSegments = lBSegmentsCombo->currentIndex();
		config.layerC.numberOfSegments = lCSegmentsCombo->currentIndex();
		config.layerA.modulation = lAModulationCombo->currentIndex();
		config.layerB.modulation = lBModulationCombo->currentIndex();
		config.layerC.modulation = lCModulationCombo->currentIndex();
		config.layerA.codeRate = lARateCombo->currentIndex();
		config.layerB.codeRate = lBRateCombo->currentIndex();
		config.layerC.codeRate = lCRateCombo->currentIndex();
		
		for(int i = 0; i < layerAList->count(); ++i) {
			QListWidgetItem* item = layerAList->item(i);
			config.layerA.programs.push_back(item->text().toUInt());
		}

		for(int i = 0; i < layerBList->count(); ++i) {
			QListWidgetItem* item = layerBList->item(i);
			config.layerB.programs.push_back(item->text().toUInt());
		}

		for(int i = 0; i < layerCList->count(); ++i) {
			QListWidgetItem* item = layerCList->item(i);
			config.layerC.programs.push_back(item->text().toUInt());
		}
		config.programMap = mPrograms;
		
		emit configReadySignal(config);
	}

	void fillTSParseResult(const std::set<ProgramInformation>& programs) 
	{
		mPrograms = programs;
		std::set<ProgramInformation>::const_iterator it;
		for(it = programs.begin(); it != programs.end(); ++it) {			
			programList->addItem(QString::number(it->program_number));
		}
	}

private:
	IRemux* mRemux;
	std::set<ProgramInformation> mPrograms;
};
