#pragma once
#include "Conmon.h"

class TSParser 
{
public:
	void read(unsigned char* aBuffer) {
	    mHeader.sync_byte = aBuffer[0];
		ASSERT_THROW(mHeader.sync_byte == 0x47, InvalidFileException());
		mHeader.transport_error_indicator = (aBuffer[1] & 0x80) >> 7;
		mHeader.payload_start_indicator = (aBuffer[1] & 0x40) >> 6;
		mHeader.transport_priority = (aBuffer[1] & 0x20) >> 5;
		mHeader.PID = ((aBuffer[1] & 0x1F) << 8) | aBuffer[2];
		mHeader.transport_scrambling_control = (aBuffer[3] & 0xC0) >> 6;
		mHeader.adaption_field_control = (aBuffer[3] & 0x30) >> 4;
		mHeader.continuity_counter = aBuffer[3] & 0xF;
		//mHeader.adaption_field_length = aBuffer[4]; 
		//mHeader.adaptation_field.discontinuity_indicator = (aBuffer[5] & 0x80) >> 7;
	}

	void write(unsigned char* aBuffer) {
		aBuffer[0] = mHeader.sync_byte;

		aBuffer[1] = mHeader.transport_error_indicator << 7 | mHeader.payload_start_indicator << 6 |
			         mHeader.transport_priority << 5 | (mHeader.PID & 0x1F00) >> 8;

		aBuffer[2] = mHeader.PID & 0xFF;

		aBuffer[3] = mHeader.transport_scrambling_control << 6 | mHeader.adaption_field_control << 4 |
			         mHeader.continuity_counter;
		//aBuffer[4] = mHeader.adaption_field_length;

		//aBuffer[5] = mHeader.adaptation_field.discontinuity_indicator << 7;
	}

	size_t getRequiredBuffer() const { return 4; }

	TS_header mHeader;

};