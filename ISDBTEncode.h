#pragma once
#include "ConfigParser.h"
#include "TSdecode.h"
#include <bitset>

///< The standard
using namespace std;

/**********************************************************************************************//**
 * \class	ISDBTEncode
 *
 * \brief   Clase encargada de codificar el BTS
 *
 **************************************************************************************************/

class ISDBTEncode
{
public:

	/**********************************************************************************************//**
	 * \fn	ISDBTEncode::ISDBTEncode(TSdecode& tsDecode, const string& btsFileName,
	 * 		const string& configFileName);
	 *
	 * \brief	Constructor
	 *
	 * \param [in,out]	tsDecode	Decodificador TS
	 * \param	btsFileName			Archivo BTS a crear
	 * \param	configFileName  	Archivo de configuracion
	 **************************************************************************************************/

	ISDBTEncode(TSdecode& tsDecode, const string& btsFileName, const string& configFileName);
	
private:

	/**********************************************************************************************//**
	 * \fn	bitset<8*8> ISDBTEncode::createISDBTInfo(unsigned int pid);
	 *
	 * \brief	Crea la informacion de ISDBT en bits
	 *
	 * \param	pid	Numero pid.
	 *
	 * \return	null if it fails, else.
	 **************************************************************************************************/

	bitset<8*8> createISDBTInfo(unsigned int pid);

	/**********************************************************************************************//**
	 * \fn	bitset<4> ISDBTEncode::getCountDownIndex();
	 *
	 * \brief	Crea el indice count_down de la norma ISDBT
	 *
	 * \return	Indice count_down
	 **************************************************************************************************/

	bitset<4> getCountDownIndex();

	/**********************************************************************************************//**
	 * \fn	bitset<4> ISDBTEncode::getHierarchy(unsigned int pid);
	 *
	 * \brief	Obtiene la jerarquia a la cual pretenece un PID en particular
	 *
	 * \param	pid PID
	 *
	 * \return Tipo de Jerarquia
	 **************************************************************************************************/

	bitset<4> getHierarchy(unsigned int pid); 

	/**********************************************************************************************//**
	 * \fn	bitset<13> ISDBTEncode::getPacketCount();
	 *
	 * \brief	Obtiene el packet_count de la norma ISDBT
	 *
	 * \return	Cantidad de paquetes
	 **************************************************************************************************/

	bitset<13> getPacketCount();	

	ConfigParser config;
	///< Decoder de TS
	TSdecode& tsDecode;
	///< Archivo de BTS a crear
	ofstream btsFile;
};