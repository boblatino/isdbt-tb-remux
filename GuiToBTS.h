#pragma once
#include "IIPStructs.h"
#include "IIPParser.h"
#include <vector>
#include <set>
#include "Conmon.h"

struct GuiLayerInfo {
	int modulation;
	int codeRate;
	int numberOfSegments;
	std::vector<size_t> programs;
};

struct GuiInfo {
	int mode;
	int guard;
	GuiLayerInfo layerA;
	GuiLayerInfo layerB;
	GuiLayerInfo layerC;
	std::set<ProgramInformation> programMap;
};


class GuiToBTS {
public:
	void convert(const GuiInfo& guiInfo);

	IIP iip;
	std::vector<size_t> pidLA;
	std::vector<size_t> pidLB;
	std::vector<size_t> pidLC;
};