#include "FileBuffer.h"



FileBuffer::FileBuffer( const char * filename, ios_base::openmode mode ) : 

						std::fstream(filename, mode), mCurrentCount(0), mBuffer(NULL)

{

	if((mode & std::ios::in) == std::ios::in) {

		initializeRead();

	} else if ((mode & std::ios::out) == std::ios::out) {

		initializeWrite();

	} else {

		assert("Invalid open type" && false);

	}



}



size_t FileBuffer::read( unsigned char* buffer, size_t size )

{

	// Limit the max read to MAX_BUFFER_SIZE

	assert(size <= MAX_BUFFER_SIZE);

	// just read the buffer if we have already allocated that size

	size_t delta = mMaxCount - mCurrentCount;

	if (size <= delta) {

		memcpy(buffer, mBuffer + mCurrentCount, size);

		mCurrentCount += size;

		return size;

	} else {

		memcpy(buffer, mBuffer + mCurrentCount, delta);

		std::fstream::read(reinterpret_cast<char*>(mBuffer), MAX_BUFFER_SIZE);

		mMaxCount = static_cast<size_t>(gcount());

		size_t copiedCount = size - delta < mMaxCount ? size - delta : mMaxCount;

		memcpy(buffer + delta, mBuffer, copiedCount);

		mCurrentCount = copiedCount;

		return copiedCount + delta;

	}

}



void FileBuffer::write( unsigned char* buffer, size_t size )

{

	// Limit the max write to MAX_BUFFER_SIZE

	assert(size <= MAX_BUFFER_SIZE);

	size_t delta = mMaxCount - mCurrentCount;

	if (size <= delta) {

		memcpy(mBuffer + mCurrentCount, buffer, size);

		mCurrentCount += size;

	} else {

		memcpy(mBuffer + mCurrentCount, buffer, delta);

		std::fstream::write(reinterpret_cast<char*>(mBuffer), MAX_BUFFER_SIZE);

		size_t reamaining = size - delta;

		memcpy(mBuffer, buffer + delta, reamaining);

		mCurrentCount = reamaining;

	}

}



FileBuffer::~FileBuffer()

{

	flushWrite();

	delete[] mBuffer;

	close();

}



void FileBuffer::seekg( streamoff off )

{

	size_t size = static_cast<size_t>(off);

	size_t delta = mMaxCount - mCurrentCount;

	if(size == 0) {

		// Special case, start over again

		mCurrentCount = 0;

		std::fstream::clear();

		std::fstream::seekg(0);

		delete[] mBuffer;

		initializeRead();

		return;

	}

	

	if(size <= delta) {

		mCurrentCount += size;

	} else {

		std::fstream::read(reinterpret_cast<char*>(mBuffer), MAX_BUFFER_SIZE);

		mMaxCount = static_cast<size_t>(gcount());

		size_t copiedCount = size - delta < mMaxCount ? size - delta : mMaxCount;

		mCurrentCount = copiedCount;

	}

}



size_t FileBuffer::fileSize()

{

	std::fstream::seekg(0, std::ios::end);

	const streampos size = tellg();

	std::fstream::seekg(0);

	return static_cast<size_t>(size);

}



bool FileBuffer::is_open() const

{

	return std::fstream::is_open();

}



bool FileBuffer::eof() const

{

	return std::fstream::eof() && (mMaxCount == mCurrentCount);

}



void FileBuffer::initializeRead()

{

	mFileSize = fileSize();

	if(mFileSize <= MAX_BUFFER_SIZE) {

		mBuffer = new unsigned char[mFileSize];

		std::fstream::read(reinterpret_cast<char*>(mBuffer), mFileSize);

		mMaxCount = static_cast<size_t>(gcount());

		assert(mMaxCount == mFileSize);

	} else {

		mBuffer = new unsigned char[MAX_BUFFER_SIZE];

		std::fstream::read(reinterpret_cast<char*>(mBuffer), MAX_BUFFER_SIZE);

		mMaxCount = static_cast<size_t>(gcount());

		assert(mMaxCount == MAX_BUFFER_SIZE);

	}

}



void FileBuffer::initializeWrite()

{

	mBuffer = new unsigned char[MAX_BUFFER_SIZE];

	mMaxCount = MAX_BUFFER_SIZE;

}



void FileBuffer::flushWrite()

{

	std::fstream::write(reinterpret_cast<char*>(mBuffer), mCurrentCount);

	mCurrentCount = 0;

}

