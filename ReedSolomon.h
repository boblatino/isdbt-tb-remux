#pragma once 
#include <memory>

#include "ShifraHeaders/schifra_galois_field.hpp"
#include "ShifraHeaders/schifra_galois_field_polynomial.hpp"

#include <boost/threadpool.hpp>

class ReedSolomon {
public:
	virtual void encode(unsigned char* buffer) = 0;
};

class Shifra : public ReedSolomon {
public:
	Shifra();
	virtual ~Shifra() {}
	void encode(unsigned char* buffer);
	void wait();

private:
	std::auto_ptr<schifra::galois::field> field;
	std::auto_ptr<schifra::galois::field_polynomial> generator_polynomial;
	std::auto_ptr<boost::threadpool::pool> threadPool;
};
