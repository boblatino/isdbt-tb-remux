#include <cstddef>
#include <iostream>
#include <string>

#include "ShifraHeaders/schifra_sequential_root_generator_polynomial_creator.hpp"
#include "ShifraHeaders/schifra_reed_solomon_encoder.hpp"
#include "ShifraHeaders/schifra_reed_solomon_block.hpp"

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread/thread.hpp>

#include "Conmon.h"
#include "ReedSolomon.h"

template<typename Encoder>
class ReedSolomonProcess
{
public:
   ReedSolomonProcess(const unsigned int& process_id,
                   boost::shared_ptr<Encoder>& encoder,
				   unsigned char* data)
   : process_id_(process_id),
     encoder_(encoder),
     mData(data){}

   ReedSolomonProcess& operator=(const ReedSolomonProcess& ep)
   {
      process_id_ = ep.process_id_;
   }

   void execute()
   {
		const std::size_t data_length = Encoder::trait::data_length;
		const std::size_t data_size   = data_length;

		typedef schifra::reed_solomon::block<code_length,fec_length> block_type;
		block_type block;

		schifra::reed_solomon::copy<unsigned char,code_length,fec_length>(mData, block);	
		encoder_->encode(block);
		
		for(size_t i = 196 ; i < 204; ++i){
			mData[i] = block[i];
		}
	}

private:
   unsigned int process_id_;
   boost::shared_ptr<Encoder> encoder_;
   unsigned char* mData;
};

const std::size_t code_length = 204;
const std::size_t fec_length = 8;
const unsigned int max_thread_count = 4; // number of functional cores.

Shifra::Shifra()
{
	/* Finite Field Parameters */
	const std::size_t field_descriptor                 =   8;
	const std::size_t generator_polynommial_index      =   0;
	const std::size_t generator_polynommial_root_count =   8;

	/* Instantiate Finite Field and Generator Polynomials */
	field.reset(new schifra::galois::field(field_descriptor,
		schifra::galois::primitive_polynomial_size05,
		schifra::galois::primitive_polynomial05));

	generator_polynomial.reset(new schifra::galois::field_polynomial(*field));
	schifra::sequential_root_generator_polynomial_creator(*field,
		generator_polynommial_index,
		generator_polynommial_root_count,
		*generator_polynomial);

	threadPool.reset(new boost::threadpool::pool(max_thread_count));
}

void Shifra::encode( unsigned char* buffer )
{
	// if we have too much tasks pending just wait
	if (threadPool->pending() > 5) {
		threadPool->wait(0);
	}
	typedef schifra::reed_solomon::shortened_encoder<code_length,fec_length> encoder_type;
	typedef ReedSolomonProcess<encoder_type> process_type;
	typedef boost::shared_ptr<process_type> process_ptr_type;
	
	boost::shared_ptr<encoder_type> encoder(new encoder_type(*field,*generator_polynomial));
	process_ptr_type process(new process_type(0,encoder, buffer));
	threadPool->schedule(boost::bind(&process_type::execute, process));
}

void Shifra::wait() {
	threadPool->wait();
}