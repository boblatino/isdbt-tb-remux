#pragma once
#include "IIPStructs.h"
#include "crc.h"
#include <memory>
#include <assert.h>

class IIPParser {
public:
	IIPParser() {}

	// Precondition, buffer needs to be 184 bytes 
	void read(unsigned char* aBuffer); // Used in read (184)

	// Get the data in byte array form
	void write(unsigned char* aBuffer);

	unsigned int calculateCRC() {
		return ::calculateCRC(mBuffer + 2, 16);
	}

	IIP mIip;
private:
	unsigned char mBuffer[184];
};