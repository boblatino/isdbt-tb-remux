#include "RemuxDecoupleThread.h"

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <queue>
#include <boost/any.hpp>

template <class T>
class BlockingQueue {
public:
	void push(const T& element){
		boost::mutex::scoped_lock lock(mMutex);
		const bool was_empty = mQueue.empty();
		mQueue.push(element);
		lock.unlock(); // unlock the mutex
		if(was_empty) {
			condVariable.notify_one();
		}		
	}

	T get() {
		boost::mutex::scoped_lock lock(mMutex);
		while(mQueue.empty()) {		
			condVariable.wait(lock);
		}
		T ret = mQueue.front();
		mQueue.pop();
		return ret;
	}

private:
	std::queue<T> mQueue;
	boost::mutex mMutex;
	boost::condition_variable condVariable;
};

struct CallInfo {
	enum { ISDBT, CONVERT_TO_TS, TS, CONVERT_TO_BTS } type;
	boost::any buffer;
	boost::any buffer2;
};

RemuxDecoupleThread::RemuxDecoupleThread(IRemux& remux, RemuxNotifier& notifier) : mRemux(remux), mNotifier(notifier) {
	mQueue.reset(new BlockingQueue<CallInfo>);
	mThread.reset(new boost::thread(boost::ref(*this)));
}

void RemuxDecoupleThread::operator()()  
{
	while(true) {	
		CallInfo info = mQueue->get();
		switch(info.type) {
		case CallInfo::ISDBT:
		{	
			ISDBTParseResult parseResult;
			mRemux.parseISDBTFile(parseResult);
			mNotifier.parseISDBTFileResult(parseResult);
			break;
		}
		case CallInfo::CONVERT_TO_TS:
			mRemux.convertToTS(boost::any_cast<std::string>(info.buffer));
			mNotifier.convertToTSFinished();
			break;
		case CallInfo::TS:
		{	
			std::set<ProgramInformation> programInformation;
			mRemux.parseTs(programInformation);
			mNotifier.parseTsResult(programInformation);
			break;
		}
		case CallInfo::CONVERT_TO_BTS:
			mRemux.convertToBTS(boost::any_cast<GuiInfo>(info.buffer), boost::any_cast<std::string>(info.buffer2));
			mNotifier.convertToBTSFinished();
			break;
		}
	}
}

void RemuxDecoupleThread::parseISDBTFile( ISDBTParseResult& result ) 
{
	CallInfo info;
	info.type = CallInfo::ISDBT;
	info.buffer = result;
	mQueue->push(info);
}

void RemuxDecoupleThread::convertToTS( const std::string& writeFileName ) 
{
	CallInfo info;
	info.type = CallInfo::CONVERT_TO_TS;
	info.buffer = writeFileName;
	mQueue->push(info);
}

void RemuxDecoupleThread::parseTs( std::set<ProgramInformation>& programs ) 
{
	CallInfo info;
	info.type = CallInfo::TS;
	info.buffer = programs;
	mQueue->push(info);
}

void RemuxDecoupleThread::convertToBTS( const GuiInfo& guiInfo, const std::string& writeFileName ) 
{
	CallInfo info;
	info.type = CallInfo::CONVERT_TO_BTS;
	info.buffer = guiInfo;
	info.buffer2 = writeFileName;
	mQueue->push(info);
}