#pragma once
#include "IRemux.h"
#include <fstream>
#include <memory> 
#include "TSParser.h"
#include "Conmon.h"
#include "FileBuffer.h"
#include <set>

struct GuiInfo;
struct ProgramInformation;

class Remux : public IRemux {
public:
	Remux();
	void open(const std::string& fileName);

	void parseISDBTFile(ISDBTParseResult& result);
	void convertToTS(const std::string& writeFileName);
	void parseTs(std::set<ProgramInformation>& programs);
	void convertToBTS(const GuiInfo& guiInfo, const std::string& writeFileName);

	typedef std::vector<size_t> pidList;
	void getPidsFromGui( const pidList &layerAPrograms, const GuiInfo &guiInfo, pidList &layerAPids );

	FILE_TYPE getFileType();
	const std::string& getFilename() const { return mFilename; }

private:
	std::auto_ptr<FileBuffer> mFile;
	std::string mFilename;
	unsigned int btsIIPFrameValues[3][4];

};