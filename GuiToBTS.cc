#include "GuiToBTS.h"

void GuiToBTS::convert( const GuiInfo& guiInfo )
{
	unsigned char mode = guiInfo.mode + 1;
	unsigned char guard = guiInfo.guard;
	unsigned char segmentsA = guiInfo.layerA.numberOfSegments;
	unsigned char segmentsB = guiInfo.layerB.numberOfSegments;
	unsigned char segmentsC = guiInfo.layerC.numberOfSegments;		

	pidLA = guiInfo.layerA.programs;
	pidLB = guiInfo.layerB.programs;
	pidLC = guiInfo.layerC.programs;

	IIPParser parser;
	iip.IIP_packet_pointer = 0;
	iip.mcci.TMCC_synchronization_word = 0;
	iip.mcci.AC_data_effective_position = 1; 
	iip.mcci.reserved = 3; //11

	iip.mcci.mgi.initialization_timing_indicator = 15;
	iip.mcci.mgi.current_mode = mode;
	iip.mcci.mgi.current_guard_interval = guard;
	iip.mcci.mgi.next_mode = mode;
	iip.mcci.mgi.next_guard_interval = guard;

	iip.mcci.tmccInformation.system_identifier = 0;
	iip.mcci.tmccInformation.count_down_index = 15;
	iip.mcci.tmccInformation.switch_on_control_flag_used_for_alert_broadcasting = 0;

	iip.mcci.tmccInformation.currentConfiguration.layerA.modulation_scheme = guiInfo.layerA.modulation;
	iip.mcci.tmccInformation.currentConfiguration.layerA.coding_rate_of_inner_code = guiInfo.layerA.codeRate;
	iip.mcci.tmccInformation.currentConfiguration.layerA.length_of_time_interleaving = 0; 
	iip.mcci.tmccInformation.currentConfiguration.layerA.number_of_segments = segmentsA;

	iip.mcci.tmccInformation.currentConfiguration.layerB.modulation_scheme =  guiInfo.layerB.modulation;
	iip.mcci.tmccInformation.currentConfiguration.layerB.coding_rate_of_inner_code =  guiInfo.layerB.codeRate;
	iip.mcci.tmccInformation.currentConfiguration.layerB.length_of_time_interleaving = 0;
	iip.mcci.tmccInformation.currentConfiguration.layerB.number_of_segments = segmentsB;

	iip.mcci.tmccInformation.currentConfiguration.layerC.modulation_scheme =  guiInfo.layerC.modulation;
	iip.mcci.tmccInformation.currentConfiguration.layerC.coding_rate_of_inner_code =  guiInfo.layerC.codeRate;
	iip.mcci.tmccInformation.currentConfiguration.layerC.length_of_time_interleaving = 0;
	iip.mcci.tmccInformation.currentConfiguration.layerC.number_of_segments = segmentsC;

	//Next configuration information
	iip.mcci.tmccInformation.nextConfiguration.partial_reception_flag = 1;
	iip.mcci.tmccInformation.nextConfiguration.layerA = iip.mcci.tmccInformation.currentConfiguration.layerA;
	iip.mcci.tmccInformation.nextConfiguration.layerB = iip.mcci.tmccInformation.currentConfiguration.layerB;
	iip.mcci.tmccInformation.nextConfiguration.layerC = iip.mcci.tmccInformation.currentConfiguration.layerC;

	iip.mcci.tmccInformation.phase_correction_of_CP_in_connected_transmission = 7;
	iip.mcci.tmccInformation.TMCC_reserved_future_use = 4095;
	iip.mcci.tmccInformation.reserved_future_use = 1023;
	iip.mcci.CRC_32 = parser.calculateCRC();

	iip.IIP_branch_number = 0;
	iip.last_IIP_branch_number = 0;
	iip.network_synchronization_information_lenght = 1;
	iip.nsi.synchronization_id =  0xFF;

	memset(iip.nsi.stuffing_byte, 0xFF,  16);
}
