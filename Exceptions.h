#pragma once
#include <exception>

class NotfoundException : public std::exception {};
class ParsingException : public std::exception {};
class InvalidFileException : public std::exception {};