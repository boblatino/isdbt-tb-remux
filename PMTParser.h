#pragma once
#include "Conmon.h"
#include <map>
#include <assert.h>

struct PMT_header {
	unsigned char table_id;
	unsigned int section_length;
	unsigned int program_number; 
	unsigned char current_next_indicator;
	unsigned char section_number;
	unsigned char last_section_number;
	unsigned int program_info_length;
	unsigned int es_info_length;

	// N of this
	unsigned char stream_type;
	unsigned int elementary_PID; 
}; 

class PMTParser {
public:
	PMTParser(unsigned int programNumber) : mProgramNumber(programNumber)
	{
	}

	void read(unsigned char* aBuffer) {
		PMT_header header;
		header.table_id = aBuffer[0];
		assert(header.table_id == 0x2);
		header.section_length = ((aBuffer[1] & 0xF) << 8) | aBuffer[2];
		header.program_number = (aBuffer[3] << 8) | aBuffer[4];
		assert(mProgramNumber == header.program_number);
		header.current_next_indicator = aBuffer[5] & 0x01;
		header.section_number = aBuffer[6];
		header.last_section_number = aBuffer[7];
		header.program_info_length = ((aBuffer[10] & 0xF) << 8) | aBuffer[11];

		if (header.current_next_indicator != 0x1) {
			return;
		}

		// Skip the descriptors (if any).
		size_t ndx = 12 + header.program_info_length;

		// Now we have the actual program data.
		mProgramInformation.program_number = header.program_number;
		while (ndx < header.section_length - 4)
		{
			header.stream_type = aBuffer[ndx++];		
			header.elementary_PID = (aBuffer[ndx++] & 0x1f) << 8;
			header.elementary_PID |= aBuffer[ndx++];
			switch (header.stream_type)
			{
			case 0x01:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, MPEG1_VIDEO));				
				break;
			case 0x02:
			case 0x1B:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, MPEG2_VIDEO));				
				break;
			case 0x03:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, MPEG1_AUDIO));				
				break;
			case 0x04:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, MPEG2_AUDIO));				
				break;
			case 0x05:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, PRIVATE_SECTION));				
				break;				
			case 0x07:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, TELETEXT_SUBS));				
				break;
			case 0x0f:
			case 0x11:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, AAC_AUDIO));				
				break;
			case 0x80:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, PRIVATE_STREAM));				
				break;
			case 0x81:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, AC3_DTS_AUDIO));				
				break;
				// These are found on bluray disks.
			case 0x85:
			case 0x86:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, DTS_AUDIO));				
				break;
			default:
				mProgramInformation.pidAndType.insert(mProgramInformation.pidAndType.end(), std::make_pair(header.elementary_PID, OTHER));				
				break;
			}
			//Skip descriptors
			header.es_info_length = (aBuffer[ndx++] & 0x0f) << 8;
			header.es_info_length |= aBuffer[ndx++];
			ndx += header.es_info_length;
		}

	}

	ProgramInformation mProgramInformation;
private:
	unsigned int mProgramNumber;
};