#include "IIPParser.h"


// Remove this asserts to write down the info not read
#undef assert
#define assert(x)

void IIPParser::write( unsigned char* aBuffer )

{

	aBuffer[0] = (mIip.IIP_packet_pointer & 0xFF00) >> 8;

	assert(memcmp(&aBuffer[0], &mBuffer[0], sizeof(char)) == 0);



	aBuffer[1] = (mIip.IIP_packet_pointer & 0xFF);

	assert(memcmp(&aBuffer[1], &mBuffer[1], sizeof(char)) == 0);



	aBuffer[2] = mIip.mcci.TMCC_synchronization_word << 7 | mIip.mcci.AC_data_effective_position << 6 |

		mIip.mcci.reserved	<< 4 | mIip.mcci.mgi.initialization_timing_indicator;

	assert(memcmp(&aBuffer[2], &mBuffer[2], sizeof(char)) == 0);



	aBuffer[3] = mIip.mcci.mgi.current_mode << 6 | mIip.mcci.mgi.current_guard_interval << 4 |

		mIip.mcci.mgi.next_mode << 2 | mIip.mcci.mgi.next_guard_interval;

	assert(memcmp(&aBuffer[3], &mBuffer[3], sizeof(char)) == 0);



	aBuffer[4] = mIip.mcci.tmccInformation.system_identifier << 6 | mIip.mcci.tmccInformation.count_down_index << 2 | 

		mIip.mcci.tmccInformation.switch_on_control_flag_used_for_alert_broadcasting << 1 | 0x1; //Asumo q el bit q pierdo es 1

	assert(memcmp(&aBuffer[4], &mBuffer[4], sizeof(char)) == 0);





	aBuffer[5] = mIip.mcci.tmccInformation.currentConfiguration.layerA.modulation_scheme << 5 |

		mIip.mcci.tmccInformation.currentConfiguration.layerA.coding_rate_of_inner_code << 2 |

		(mIip.mcci.tmccInformation.currentConfiguration.layerA.length_of_time_interleaving & 0x3) >> 1;

	assert(memcmp(&aBuffer[5], &mBuffer[5], sizeof(char)) == 0);





	aBuffer[6] = (mIip.mcci.tmccInformation.currentConfiguration.layerA.length_of_time_interleaving & 0x80) << 7 |

		mIip.mcci.tmccInformation.currentConfiguration.layerA.number_of_segments << 3 |

		mIip.mcci.tmccInformation.currentConfiguration.layerB.modulation_scheme;

	assert(memcmp(&aBuffer[6], &mBuffer[6], sizeof(char)) == 0);



	aBuffer[7] = mIip.mcci.tmccInformation.currentConfiguration.layerB.coding_rate_of_inner_code << 5 |

		mIip.mcci.tmccInformation.currentConfiguration.layerB.length_of_time_interleaving << 2 |

		(mIip.mcci.tmccInformation.currentConfiguration.layerB.number_of_segments & 0xC) >> 2;

	assert(memcmp(&aBuffer[7], &mBuffer[7], sizeof(char)) == 0);



	aBuffer[8] = (mIip.mcci.tmccInformation.currentConfiguration.layerB.number_of_segments & 0x3) << 6 |

		mIip.mcci.tmccInformation.currentConfiguration.layerC.modulation_scheme << 3 |

		mIip.mcci.tmccInformation.currentConfiguration.layerC.coding_rate_of_inner_code;

	assert(memcmp(&aBuffer[8], &mBuffer[8], sizeof(char)) == 0);



	aBuffer[9] = mIip.mcci.tmccInformation.currentConfiguration.layerC.length_of_time_interleaving << 5 |

		mIip.mcci.tmccInformation.currentConfiguration.layerC.number_of_segments << 1 |

		mIip.mcci.tmccInformation.nextConfiguration.partial_reception_flag;

	assert(memcmp(&aBuffer[9], &mBuffer[9], sizeof(char)) == 0);



	aBuffer[10] = mIip.mcci.tmccInformation.nextConfiguration.layerA.modulation_scheme << 5 |

		mIip.mcci.tmccInformation.nextConfiguration.layerA.coding_rate_of_inner_code << 2 |

		(mIip.mcci.tmccInformation.nextConfiguration.layerA.length_of_time_interleaving & 0x6) >> 1;

	assert(memcmp(&aBuffer[10], &mBuffer[10], sizeof(char)) == 0);



	aBuffer[11] = mIip.mcci.tmccInformation.nextConfiguration.layerA.length_of_time_interleaving & 0x1 << 7 |

		mIip.mcci.tmccInformation.nextConfiguration.layerA.number_of_segments << 3 |

		mIip.mcci.tmccInformation.nextConfiguration.layerB.modulation_scheme;

	assert(memcmp(&aBuffer[11], &mBuffer[11], sizeof(char)) == 0);



	aBuffer[12] = mIip.mcci.tmccInformation.nextConfiguration.layerB.coding_rate_of_inner_code << 5 | 

		mIip.mcci.tmccInformation.nextConfiguration.layerB.length_of_time_interleaving << 2 |

		(mIip.mcci.tmccInformation.nextConfiguration.layerB.number_of_segments & 0xC) >> 2;

	assert(memcmp(&aBuffer[12], &mBuffer[12], sizeof(char)) == 0);



	aBuffer[13] = (mIip.mcci.tmccInformation.nextConfiguration.layerB.number_of_segments & 0x3) << 6 |

		mIip.mcci.tmccInformation.nextConfiguration.layerC.modulation_scheme << 3 | 

		mIip.mcci.tmccInformation.nextConfiguration.layerC.coding_rate_of_inner_code;

	assert(memcmp(&aBuffer[13], &mBuffer[13], sizeof(char)) == 0);



	aBuffer[14] = mIip.mcci.tmccInformation.nextConfiguration.layerC.length_of_time_interleaving << 5 | 

		mIip.mcci.tmccInformation.nextConfiguration.layerC.number_of_segments << 1 |

		(mIip.mcci.tmccInformation.phase_correction_of_CP_in_connected_transmission & 0x4) >> 2;

	assert(memcmp(&aBuffer[14], &mBuffer[14], sizeof(char)) == 0);



	aBuffer[15] = (mIip.mcci.tmccInformation.phase_correction_of_CP_in_connected_transmission & 0x3) << 6 |

		(mIip.mcci.tmccInformation.TMCC_reserved_future_use & 0xFC0) >> 6;

	assert(memcmp(&aBuffer[15], &mBuffer[15], sizeof(char)) == 0);



	aBuffer[16] = (mIip.mcci.tmccInformation.TMCC_reserved_future_use & 0x3F) << 2 |

		(mIip.mcci.tmccInformation.reserved_future_use & 0x300) >> 8;

	assert(memcmp(&aBuffer[16], &mBuffer[16], sizeof(char)) == 0);



	aBuffer[17] = mIip.mcci.tmccInformation.reserved_future_use & 0xFF; 

	assert(memcmp(&aBuffer[17], &mBuffer[17], sizeof(char)) == 0);



	aBuffer[18] = (mIip.mcci.CRC_32 & 0xFF000000) >> 24; 

	assert(memcmp(&aBuffer[18], &mBuffer[18], sizeof(char)) == 0);



	aBuffer[19] = (mIip.mcci.CRC_32 & 0xFF0000) >> 16; 

	assert(memcmp(&aBuffer[19], &mBuffer[19], sizeof(char)) == 0);



	aBuffer[20] = (mIip.mcci.CRC_32 & 0xFF00) >> 8; 

	assert(memcmp(&aBuffer[20], &mBuffer[20], sizeof(char)) == 0);



	aBuffer[21] = mIip.mcci.CRC_32 & 0xFF;

	assert(memcmp(&aBuffer[21], &mBuffer[21], sizeof(char)) == 0);



	aBuffer[22] = mIip.IIP_branch_number;

	assert(memcmp(&aBuffer[22], &mBuffer[22], sizeof(char)) == 0);



	aBuffer[23] = mIip.last_IIP_branch_number;

	assert(memcmp(&aBuffer[23], &mBuffer[23], sizeof(char)) == 0);



	aBuffer[24] = mIip.network_synchronization_information_lenght;

	assert(memcmp(&aBuffer[24], &mBuffer[24], sizeof(char)) == 0);



	aBuffer[25] = mIip.nsi.synchronization_id;

	assert(memcmp(&aBuffer[25], &mBuffer[25], sizeof(char)) == 0);



	// Stuffin byte (basicamente relleno ya q no hay info de FU)				

	// 128 bits deberia ser el relleno si es que es la suma total de FU

	size_t count = 0;

	for(size_t i = 26 ; i < 26+16 ; i++) {

		aBuffer[i] = mIip.nsi.stuffing_byte[count];

		assert(memcmp(&aBuffer[i], &mBuffer[i], sizeof(char)) == 0);

		count++;

	}	



	assert(memcmp(aBuffer, mBuffer, sizeof(char) * 41) == 0);

}



void IIPParser::read( unsigned char* aBuffer )

{

	memcpy(mBuffer, aBuffer, sizeof(mBuffer));



	mIip.IIP_packet_pointer = (aBuffer[0] << 8) | aBuffer[1];



	mIip.mcci.TMCC_synchronization_word = (aBuffer[2] & 0x80) >> 7; // OFDM W0 or W1

	mIip.mcci.AC_data_effective_position = (aBuffer[2] & 0x40) >> 6; 

	mIip.mcci.reserved = (aBuffer[2] & 0x30) >> 4; // dont care



	mIip.mcci.mgi.initialization_timing_indicator = (aBuffer[2] & 0xF);

	mIip.mcci.mgi.current_mode = (aBuffer[3] & 0xC0) >> 6;

	mIip.mcci.mgi.current_guard_interval = (aBuffer[3] & 0x30) >> 4;

	mIip.mcci.mgi.next_mode = (aBuffer[3] & 0xC) >> 2;

	mIip.mcci.mgi.next_guard_interval = (aBuffer[3] & 0x3);



	mIip.mcci.tmccInformation.system_identifier =  (aBuffer[4] & 0xC0) >> 6;

	mIip.mcci.tmccInformation.count_down_index = (aBuffer[4] & 0x3C) >> 2;

	mIip.mcci.tmccInformation.switch_on_control_flag_used_for_alert_broadcasting = (aBuffer[4] & 0x2) >> 1;



	//Pierdo un bit a proposito y en la norma no figura que es!

	mIip.mcci.tmccInformation.currentConfiguration.layerA.modulation_scheme = (aBuffer[5] & 0xE0) >> 5;

	mIip.mcci.tmccInformation.currentConfiguration.layerA.coding_rate_of_inner_code = (aBuffer[5] & 0x1C) >> 2;

	mIip.mcci.tmccInformation.currentConfiguration.layerA.length_of_time_interleaving = (aBuffer[5] & 0x3) << 1 | ((aBuffer[6] & 0x80) >> 7);

	mIip.mcci.tmccInformation.currentConfiguration.layerA.number_of_segments = (aBuffer[6] & 0x78) >> 3;



	mIip.mcci.tmccInformation.currentConfiguration.layerB.modulation_scheme = (aBuffer[6] & 0x7);

	mIip.mcci.tmccInformation.currentConfiguration.layerB.coding_rate_of_inner_code = (aBuffer[7] & 0xE0) >> 5;

	mIip.mcci.tmccInformation.currentConfiguration.layerB.length_of_time_interleaving = (aBuffer[7] & 0x1C) >> 2;

	mIip.mcci.tmccInformation.currentConfiguration.layerB.number_of_segments = (aBuffer[7] & 0x3) << 2 | ((aBuffer[8] & 0xC0) >> 6);



	mIip.mcci.tmccInformation.currentConfiguration.layerC.modulation_scheme = (aBuffer[8] & 0x38) >> 3;

	mIip.mcci.tmccInformation.currentConfiguration.layerC.coding_rate_of_inner_code = (aBuffer[8] & 0x7);

	mIip.mcci.tmccInformation.currentConfiguration.layerC.length_of_time_interleaving = (aBuffer[9] & 0xE0) >> 5;

	mIip.mcci.tmccInformation.currentConfiguration.layerC.number_of_segments = (aBuffer[9] & 0x1E) >> 1;



	//Next configuration information

	mIip.mcci.tmccInformation.nextConfiguration.partial_reception_flag = (aBuffer[9] & 0x1);			



	mIip.mcci.tmccInformation.nextConfiguration.layerA.modulation_scheme = (aBuffer[10] & 0xE0) >> 5;

	mIip.mcci.tmccInformation.nextConfiguration.layerA.coding_rate_of_inner_code = (aBuffer[10] & 0x1C) >> 2;

	mIip.mcci.tmccInformation.nextConfiguration.layerA.length_of_time_interleaving = (aBuffer[10] & 0x3) << 1 | ((aBuffer[11] & 0x80) >> 7);

	mIip.mcci.tmccInformation.nextConfiguration.layerA.number_of_segments = (aBuffer[11] & 0x78) >> 3;



	mIip.mcci.tmccInformation.nextConfiguration.layerB.modulation_scheme = (aBuffer[11] & 0x7);

	mIip.mcci.tmccInformation.nextConfiguration.layerB.coding_rate_of_inner_code = (aBuffer[12] & 0xE0) >> 5;				

	mIip.mcci.tmccInformation.nextConfiguration.layerB.length_of_time_interleaving = (aBuffer[12] & 0x1C) >> 2;

	mIip.mcci.tmccInformation.nextConfiguration.layerB.number_of_segments = (aBuffer[12] & 0x3) << 2 | ((aBuffer[13] & 0xC0) >> 6);



	mIip.mcci.tmccInformation.nextConfiguration.layerC.modulation_scheme = (aBuffer[13] & 0x38) >> 3;

	mIip.mcci.tmccInformation.nextConfiguration.layerC.coding_rate_of_inner_code = (aBuffer[13] & 0x7);

	mIip.mcci.tmccInformation.nextConfiguration.layerC.length_of_time_interleaving = (aBuffer[14] & 0xE0) >> 5;



	mIip.mcci.tmccInformation.nextConfiguration.layerC.number_of_segments = (aBuffer[14] & 0x1E) >> 1;

	//phase_shift_correction_value No dice q carajo es

	mIip.mcci.tmccInformation.phase_correction_of_CP_in_connected_transmission = (aBuffer[14] & 0x1) << 2 | ((aBuffer[15] & 0xC0) >> 6);

	// Reservado para futuro 12

	mIip.mcci.tmccInformation.TMCC_reserved_future_use = (aBuffer[15] & 0x3F) << 6 | ((aBuffer[16] & 0xFC) >> 2);

	//Reservado para futuro 10

	mIip.mcci.tmccInformation.reserved_future_use = (aBuffer[16] & 0x3) << 8 | aBuffer[17];

	//CRC de todo el mcci 32 Ver norma como calcular esto

	mIip.mcci.CRC_32 = aBuffer[18] << 24 | aBuffer[19] << 16 |aBuffer[20] << 8 | aBuffer[21];

	unsigned int crc = calculateCRC();

	assert(crc == mIip.mcci.CRC_32);



	//IIP branch number: Indica el n�mero de ramales del IIP

	mIip.IIP_branch_number = aBuffer[22];



	//Last iip branch number: Indica el �ltimo �IIP_branch_number� del sub-paquete mIip.

	mIip.last_IIP_branch_number = aBuffer[23];

	mIip.network_synchronization_information_lenght = aBuffer[24];

	// El valor anterior indica que el len del net sync information es 1 es decir q hay uno solo

	mIip.nsi.synchronization_id =  aBuffer[25];

	// Stuffin byte (basicamente relleno ya q no hay info de FU)				

	// 128 bits deberia ser el relleno si es que es la suma total de FU

	size_t count = 0;

	for(size_t i = 26 ; i < 26+16 ; i++) {

		mIip.nsi.stuffing_byte[count] = aBuffer[i];

		count++;

	}

}

