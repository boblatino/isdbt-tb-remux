#include <limits>

#include "Remux.h"
#include "ISDBTInfoParser.h"
#include "TSParser.h"
#include "PATParser.h"
#include "PMTParser.h"
#include "Conmon.h"
#include "IIPParser.h"
#include "GuiToBTS.h"
#include <algorithm>
#include "ReedSolomon.h"

#include "LieToMe.h"

FILE_TYPE Remux::getFileType()
{
	mFile->seekg(0);
	unsigned char buffer[ISDBT_PACKET_SIZE * 2];
	size_t bytesRead = mFile->read(buffer, ISDBT_PACKET_SIZE * 2);
	if(bytesRead == ISDBT_PACKET_SIZE * 2) {
		unsigned char* pos = buffer;
		// no sync byte found
		try {
			TSParser tsParser;
			tsParser.read(pos);
		} catch(const InvalidFileException& ) { return INVALID; }
		// Check if its a TS file
		try {
			pos += TS_PACKET_SIZE;
			TSParser tsParser;
			tsParser.read(pos);
			return TS;
		} catch(const InvalidFileException& ) {
			pos += (ISDBT_PACKET_SIZE - TS_PACKET_SIZE);
			try {
				TSParser tsParser;
				tsParser.read(pos);
				return BTS;
			} catch(const InvalidFileException& ) { return INVALID; }
		}	
	}
	return INVALID;
}



void Remux::parseISDBTFile(ISDBTParseResult& result)

{
	mFile->seekg(0);
	unsigned int layerACount = 0;
	unsigned int layerBCount = 0;
	unsigned int layerCCount = 0;
	unsigned int nullCount = 0;	
	unsigned int iipCount = 0;

	std::set<ProgramInformation> programs;
	size_t packetCount = 0;
	PATParser patParser;
	bool enableCount = true;

	memset(&result.config, 0, sizeof(result.config)); 

	//Shifra rs;
	while (!mFile->eof())
	{
		packetCount++;
		unsigned char buffer[ISDBT_PACKET_SIZE] = {0};
		unsigned char* readPointer = buffer;
		if(mFile->read(readPointer, ISDBT_PACKET_SIZE) != ISDBT_PACKET_SIZE) {
			break;
		}

		TSParser tsParser;		
		tsParser.read(readPointer);
		readPointer += tsParser.getRequiredBuffer();
		ASSERT_THROW(tsParser.mHeader.sync_byte == 0x47, ParsingException());

		// If there is an error, skip
		if(tsParser.mHeader.transport_error_indicator == 0x1) {
			continue;
		}

		// Skip if this has only adaptation field
		if (tsParser.mHeader.adaption_field_control == 0x2) {
			continue;
		}

		//Read 184 bytes to get the payload
		unsigned char* payload = readPointer;
		readPointer += 184;

		unsigned char* isdbtInfoBuffer = readPointer;
		readPointer += 8;

		ISDBTInfoParser isdbtParser;
		isdbtParser.read(isdbtInfoBuffer);

		//rs.encode(buffer);
		unsigned char layer = isdbtParser.mIsdbtInfo.layer_indicator;	
		
		//Null packet so discard
		if(tsParser.mHeader.PID == 0x1FFF) {  
			//result.BTSLayerOrder.append("N");
			if(enableCount) nullCount++;
			continue;			
		}
		
		// Parse PAT
		if(tsParser.mHeader.PID == 0x0) {  
			unsigned char* start = payload;
			if(tsParser.mHeader.payload_start_indicator == 0x1) {
				// Skip the pointer
				start += 1;			
			}
			if(patParser.isFilled() == false) {
				patParser.read(start);
			}		
		}

		// Parse PMTs
		if(patParser.isFilled()) {
			const std::map<unsigned int, unsigned int>& programToPid = patParser.mProgramNumberToMapPID;
			std::map<unsigned int, unsigned int>::const_iterator it = programToPid.find(tsParser.mHeader.PID);
			if(it != programToPid.end()) {  
				unsigned char* start = payload;
				if(tsParser.mHeader.payload_start_indicator == 0x1) {
					// Skip the pointer
					start += 1;			
				}
				PMTParser pmtParser(it->second);
				pmtParser.read(start);
				programs.insert(programs.end(), pmtParser.mProgramInformation);
			}
		}

		ConfigRead& config = result.config;
		//Layer A
		if(layer == 0x1) {
			if(enableCount) layerACount++;
			//result.BTSLayerOrder.append("A");
			config.isA = true;
		}

		//Layer B
		if(layer == 0x2) {
			if(enableCount) layerBCount++;
			//result.BTSLayerOrder.append("B");
			config.isB = true;
		}

		//Layer C
		if(layer == 0x3) {
			if(enableCount) layerCCount++;
			//result.BTSLayerOrder.append("C");
			config.isC = true;
		}

		// IIP packet
	/*	struct ConfigRead{
			int mode;
			int codeRate[3];
			int modulation[3];
			int guard;
		};*/

		if(layer == 0x8) {
			//result.BTSLayerOrder.append("I");
			if(enableCount) iipCount++;
			assert(tsParser.mHeader.PID == 0x1FF0); // Verificado con software externo
			IIPParser parser;
			parser.read(payload);	

			config.guard = parser.mIip.mcci.mgi.current_guard_interval;
			config.mode = parser.mIip.mcci.mgi.current_mode;
			
			config.codeRate[0] = parser.mIip.mcci.tmccInformation.currentConfiguration.layerA.coding_rate_of_inner_code;
			config.codeRate[1] = parser.mIip.mcci.tmccInformation.currentConfiguration.layerB.coding_rate_of_inner_code;
			config.codeRate[2] = parser.mIip.mcci.tmccInformation.currentConfiguration.layerC.coding_rate_of_inner_code;

			config.modulation[0] = parser.mIip.mcci.tmccInformation.currentConfiguration.layerA.modulation_scheme;
			config.modulation[1] = parser.mIip.mcci.tmccInformation.currentConfiguration.layerB.modulation_scheme;
			config.modulation[2] = parser.mIip.mcci.tmccInformation.currentConfiguration.layerC.modulation_scheme;

			config.segments[0] = parser.mIip.mcci.tmccInformation.currentConfiguration.layerA.number_of_segments;
			config.segments[1] = parser.mIip.mcci.tmccInformation.currentConfiguration.layerB.number_of_segments;
			config.segments[2] = parser.mIip.mcci.tmccInformation.currentConfiguration.layerC.number_of_segments;

			enableCount = false;
		}
	}
	result.stats.layerApacketCount = layerACount;
	result.stats.layerBpacketCount = layerBCount;
	result.stats.layerCpacketCount = layerCCount;
	result.stats.nullPacketCount = nullCount;
	result.stats.iipPacketCount = iipCount;
	result.config.fileSize = packetCount; 
}



void Remux::convertToTS(const std::string& writeFileName)

{

	mFile->seekg(0);

	FileBuffer writeFile(writeFileName.c_str(), std::ios::binary | std::ios::out);

	while (!mFile->eof())

	{

		unsigned char buffer[ISDBT_PACKET_SIZE] = {0};

		unsigned char* readPointer = buffer;

		if(mFile->read(readPointer, ISDBT_PACKET_SIZE) != ISDBT_PACKET_SIZE) {
			break;
		}

		TSParser tsParser;		

		tsParser.read(readPointer);

		readPointer += tsParser.getRequiredBuffer();

		ASSERT_THROW(tsParser.mHeader.sync_byte == 0x47, ParsingException());

		

		readPointer += 184;

		unsigned char* isdbtInfoBuffer = readPointer;

		ISDBTInfoParser isdbtParser;

		isdbtParser.read(isdbtInfoBuffer);



		unsigned char layer = isdbtParser.mIsdbtInfo.layer_indicator;



		// IIP packet so we must remove this from the file

		if(layer == 0x8) {

			continue;

		}

		writeFile.write(buffer, TS_PACKET_SIZE);

	}

}



void Remux::parseTs(std::set<ProgramInformation>& programs)
{
	mFile->seekg(0);
	PATParser patParser;
	while (!mFile->eof())
	{
		unsigned char buffer[TS_PACKET_SIZE] = {0};

		unsigned char* readPointer = buffer;

		if(mFile->read(buffer, TS_PACKET_SIZE) != TS_PACKET_SIZE) {

			break;

		}



		TSParser tsParser;		

		tsParser.read(readPointer);

		readPointer += tsParser.getRequiredBuffer();

		ASSERT_THROW(tsParser.mHeader.sync_byte == 0x47, ParsingException());



		// If there is an error, skip

		if(tsParser.mHeader.transport_error_indicator == 0x1) {

			continue;

		}



		// Skip if this has only adaptation field

		if (tsParser.mHeader.adaption_field_control == 0x2) {

			continue;

		}



		//Read 184 bytes to get the payload

		unsigned char* payload = readPointer;

		readPointer += 184;	



		//Null packet so discard

		if(tsParser.mHeader.PID == 0x1FFF) {  

			continue;

		}



		// Parse PAT

		if(tsParser.mHeader.PID == 0x0) {  

			unsigned char* start = payload;

			if(tsParser.mHeader.payload_start_indicator == 0x1) {

				// Skip the pointer

				start += 1;			

			}

			if(patParser.isFilled() == false) {

				patParser.read(start);

			}		

		}



		// Parse PMTs

		if(patParser.isFilled()) {

			const std::map<unsigned int, unsigned int>& programToPid = patParser.mProgramNumberToMapPID;

			std::map<unsigned int, unsigned int>::const_iterator it = programToPid.find(tsParser.mHeader.PID);

			if(it != programToPid.end()) {  

				unsigned char* start = payload;

				if(tsParser.mHeader.payload_start_indicator == 0x1) {

					// Skip the pointer

					start += 1;			

				}

				PMTParser pmtParser(it->second);

				pmtParser.read(start);

				programs.insert(programs.end(), pmtParser.mProgramInformation);



				// If we already got all the programs, exit

				if (programs.size() == programToPid.size()) {

					return;

				}

			}

		}		

	}	

}



void Remux::convertToBTS(const GuiInfo& guiInfo, const std::string& writeFileName)
{
	GuiToBTS guiToBts;
	guiToBts.convert(guiInfo);
	IIPParser iipParser;
	iipParser.mIip = guiToBts.iip;
	IIP& iip = iipParser.mIip;

	// USED IN IIP PACKET
	TSParser tsParser;
	TS_header& tsHeader = tsParser.mHeader;
	tsHeader.sync_byte = 0x47;
	tsHeader.transport_error_indicator = 0;
	tsHeader.payload_start_indicator = 1;
	tsHeader.transport_priority = 0;
	tsHeader.transport_scrambling_control = 0;
	tsHeader.adaption_field_control = 1;
	tsHeader.PID = 0x1FF0;
	tsHeader.continuity_counter = 0; 

	// USED IN EACH PACKET INCLUDING IIP
	ISDBTInfoParser isdbtParser;
	ISDB_information& isdbtInfo = isdbtParser.mIsdbtInfo;
	isdbtInfo.TMCC_identifier = 2;
	isdbtInfo.reserved = 1;
	isdbtInfo.buffer_reset_control_flag = 0;
	isdbtInfo.switch_on_control_flag_for_emergency_broadcasting = 0;
	isdbtInfo.initialization_timing_head_packet_flag = 0;
	isdbtInfo.frame_head_packet_flag = 0; 
	isdbtInfo.layer_indicator = 8; 
	isdbtInfo.count_down_index = 15; 
	isdbtInfo.AC_data_invalid_flag = 1;
	isdbtInfo.AC_data_effective_bytes = 3;
	const unsigned int packetsUntilIIP = btsIIPFrameValues[guiInfo.mode][guiInfo.guard];
	isdbtInfo.TSP_counter = packetsUntilIIP; 
	isdbtInfo.stuffing_bit_or_AC_data = std::numeric_limits<unsigned int>::max();	

	FileBuffer writeFile(writeFileName.c_str(), std::ios::binary | std::ios::out);
	mFile->seekg(0);
	unsigned long long packetCount = 0;	

	const size_t tsToBTSDelta = ISDBT_PACKET_SIZE - TS_PACKET_SIZE;
	unsigned char stuffing[tsToBTSDelta];
	memset(stuffing, std::numeric_limits<unsigned int>::max(), tsToBTSDelta);

	const pidList& layerAPrograms = guiInfo.layerA.programs;
	const pidList& layerBPrograms = guiInfo.layerB.programs;
	const pidList& layerCPrograms = guiInfo.layerC.programs;	

	pidList layerAPids;
	pidList layerBPids;
	pidList layerCPids;

	getPidsFromGui(layerAPrograms, guiInfo, layerAPids);
	getPidsFromGui(layerBPrograms, guiInfo, layerBPids);
	getPidsFromGui(layerCPrograms, guiInfo, layerCPids);

	size_t skipNullCount = 0;
	bool frameToggle = false;
	unsigned int iipCount = 0;
	while (!mFile->eof())
	{
		packetCount++;
		iipCount++;

		// DO NOT READ THE FILE IF WE NEED TO ADD AN IIP PACKET
		// IIP packet construction
		if(iipCount == packetsUntilIIP) { 
			iipCount = 0;
			// change the TS part
			tsHeader.continuity_counter = (tsHeader.continuity_counter + 1) & 0xF;
			// Change the corresponding isdbt part
			isdbtInfo.frame_indicator = frameToggle;
			skipNullCount++;

			// Change the iip part
			iip.mcci.TMCC_synchronization_word = frameToggle;
			frameToggle = !frameToggle;
			// store the result
			unsigned char buffer [ISDBT_PACKET_SIZE] = {0};
			unsigned char* writePtr = buffer;
			// TS header
			tsParser.write(writePtr);
			writePtr += 4;

			// Payload IIP
			iipParser.write(writePtr);
			writePtr += 184;

			// ISDBT info
			isdbtInfo.layer_indicator = 8;
			isdbtParser.write(writePtr);

			// TODO ADD THE REED SOLOMON
			writeFile.write(buffer, ISDBT_PACKET_SIZE);
			continue;
		}		

		unsigned char buffer[ISDBT_PACKET_SIZE] = {0};
		unsigned char* readPointer = buffer;
		if(mFile->read(readPointer, TS_PACKET_SIZE) != TS_PACKET_SIZE) {
			break;
		}		

		TSParser cpTsParser;
		cpTsParser.read(readPointer);
		readPointer += cpTsParser.getRequiredBuffer();

		// If there is an error, skip		
		if(cpTsParser.mHeader.transport_error_indicator == 0x1) {
			writeFile.write(buffer, TS_PACKET_SIZE);
			writeFile.write(stuffing, tsToBTSDelta);
			continue;
		}

		// Skip if this has only adaptation field
		if (cpTsParser.mHeader.adaption_field_control == 0x2) {
			writeFile.write(buffer, TS_PACKET_SIZE);
			writeFile.write(stuffing, tsToBTSDelta);
			continue;
		}

		//Read 184 bytes to get the payload
		unsigned char* payload = readPointer;
		readPointer += 184;

		unsigned char* isdbtInfoBuffer = readPointer;

		//Null packet so discard
		if(cpTsParser.mHeader.PID == 0x1FFF) { 
			if(skipNullCount == 0) {
				writeFile.write(buffer, TS_PACKET_SIZE);
				writeFile.write(stuffing, tsToBTSDelta);
			} else {
				skipNullCount--;
			}
			continue;
		}
	
		//Layer A
		pidList::const_iterator layerAIt = std::find(layerAPids.begin(), layerAPids.end(), cpTsParser.mHeader.PID);
		if(layerAIt != layerAPids.end()) {
			isdbtInfo.layer_indicator = 1;
		}

		//Layer B
		pidList::const_iterator layerBIt = std::find(layerBPids.begin(), layerBPids.end(), cpTsParser.mHeader.PID);
		if(layerBIt != layerBPids.end()) {
			isdbtInfo.layer_indicator = 2;
		}

		//Layer C
		pidList::const_iterator layerCIt = std::find(layerCPids.begin(), layerCPids.end(), cpTsParser.mHeader.PID);
		if(layerCIt != layerCPids.end()) {
			isdbtInfo.layer_indicator = 3;
		}

		// Default to something different than 8 for ts special packets
		//TODO: add PMT corresponding pid to each layer
		if(isdbtInfo.layer_indicator != 1 && isdbtInfo.layer_indicator != 2 && isdbtInfo.layer_indicator != 3)
		{
			isdbtInfo.layer_indicator = 5;
		}

		isdbtParser.write(isdbtInfoBuffer);
		writeFile.write(buffer, ISDBT_PACKET_SIZE);
	}
}



Remux::Remux()
{
	// fill table
	btsIIPFrameValues[0][0] = 1056;
	btsIIPFrameValues[1][0] = 2112;
	btsIIPFrameValues[2][0] = 4224;

	btsIIPFrameValues[0][1] = 1088;
	btsIIPFrameValues[1][1] = 2176;
	btsIIPFrameValues[2][1] = 4352;

	btsIIPFrameValues[0][2] = 1152;
	btsIIPFrameValues[1][2] = 2304;
	btsIIPFrameValues[2][2] = 4608;

	btsIIPFrameValues[0][3] = 1280;
	btsIIPFrameValues[1][3] = 2560;
	btsIIPFrameValues[2][3] = 5120;
}

void Remux::open( const std::string& fileName )
{
	mFilename = fileName;
	mFile.reset(new FileBuffer(fileName.c_str(), std::ios::binary | std::ios::in));
}

void Remux::getPidsFromGui( const pidList &layerAPrograms, const GuiInfo &guiInfo, pidList &layerAPids )
{
	for(pidList::const_iterator it = layerAPrograms.begin(); it != layerAPrograms.end(); ++it) {
		std::set<ProgramInformation>::iterator piIt = guiInfo.programMap.begin();
		for(; piIt !=  guiInfo.programMap.end(); ++piIt) {
			if(piIt->program_number == *it) {
				break;
			}
		}
		assert(piIt != guiInfo.programMap.end());
		for(std::map<unsigned int, STREAM_TYPE>::const_iterator pidIt = piIt->pidAndType.begin(); pidIt != piIt->pidAndType.end(); ++pidIt) {
			layerAPids.push_back(pidIt->first);
		}
	}
}

