#pragma once
#include <sstream>


struct PacketsInModeTable
{
	int getSetup(int mode, int codeRate, int modulation) {
		int val = table[mode-1][codeRate];
		if(modulation == 2) {
			val *= 2;
		} else if(modulation == 3) {
			val *= 3;
		}
		
		return val;
	}
	
	PacketsInModeTable() {
		table[0][0] = 12;
		table[0][1] = 16;
		table[0][2] = 18;
		table[0][3] = 20;
		table[0][4] = 21;

		table[1][0] = 24;
		table[1][1] = 32;
		table[1][2] = 36;
		table[1][3] = 40;
		table[1][4] = 42;

		table[2][0] = 48;
		table[2][1] = 64;
		table[2][2] = 72;
		table[2][3] = 80;
		table[2][4] = 84;
	}

	int table[3][5];
};

struct PacketsPerFrameTable
{
	int getSetup(int mode, int guard) {
		assert(mode-1 <= 2);
		assert(guard <= 4);
		return table[mode-1][3 - guard];
	}
	
	
	PacketsPerFrameTable() {
		table[0][0] = 1280;
        table[0][1] = 1152;
		table[0][2] = 1088;
		table[0][3] = 1056;

		table[1][0] = 2560;
		table[1][1] = 2304;
		table[1][2] = 2176;
		table[1][3] = 2112;

		table[2][0] = 5120;
		table[2][1] = 4608;
		table[2][2] = 4352;
		table[2][3] = 4224;
	}

	int table[3][4];
};


/*
layerACountLabel->setText(QString::number(result.stats.layerApacketCount));
layerBCountLabel->setText(QString::number(result.stats.layerBpacketCount));
layerCCountLabel->setText(QString::number(result.stats.layerCpacketCount));
iipCountLabel->setText(QString::number(result.stats.iipPacketCount));
nullCountLabel->setText(QString::number(result.stats.nullPacketCount));*/

struct counts {
	int layerACount;
	int layerBCount;
	int layerCCount;
	int nullCount;
	int packetsPerframeCount;
};

struct BadConfigException : public std::exception {
}; 

class LieToMe {

public:
	std::string getPrintout(const ISDBTParseResult& config) {
		std::stringstream out;
		memset(&c, 0, sizeof(counts));				

		int aSeg = config.config.isA ? config.config.segments[0] : 0;
		int bSeg = config.config.isB ? config.config.segments[1] : 0;
		int cSeg = config.config.isC ? config.config.segments[2] : 0;

		int segments =  aSeg + bSeg + cSeg;
		if(segments != 13) {
			throw BadConfigException();
		} 		
		
		int packetsInLayerA = 0;
		int packetsInLayerB = 0;
		int packetsInLayerC = 0;

		if(config.config.isA) packetsInLayerA = pim.getSetup(config.config.mode, config.config.codeRate[0], config.config.modulation[0]) * config.config.segments[0];
		if(config.config.isB) packetsInLayerB = pim.getSetup(config.config.mode, config.config.codeRate[1], config.config.modulation[1]) * config.config.segments[1];
		if(config.config.isC) packetsInLayerC = pim.getSetup(config.config.mode, config.config.codeRate[2], config.config.modulation[2]) * config.config.segments[2];

		int totalPackets = packetsInLayerA + packetsInLayerB + packetsInLayerC;

		int packetsPerframe = ppf.getSetup(config.config.mode, config.config.guard);
		c.layerACount = packetsInLayerA;
		c.layerBCount = packetsInLayerB;
		c.layerCCount = packetsInLayerC;

		int nullPackets = packetsPerframe - totalPackets;
		if(nullPackets < 0) {
			throw BadConfigException();
		}
		c.nullCount = nullPackets;

		const char letters[4] = {'A', 'B', 'C', 'N'}; 
		int* counters[4] = {&packetsInLayerA, &packetsInLayerB, &packetsInLayerC, &nullPackets};

		do {
			int randIndex = rand() % 4;
			if((*counters[randIndex]) > 0) { 
				(*counters[randIndex])--;
				out << letters[randIndex];
			}			
		} while((*counters[0]) > 0 || (*counters[1]) > 0 || (*counters[2]) > 0 || (*counters[3]) > 0);
		
		out << "I";

		c.packetsPerframeCount = packetsPerframe;
		return out.str();
	}

	counts c;

private:
	PacketsInModeTable pim;
	PacketsPerFrameTable ppf;

};