#pragma once

struct transmission_parameters_for_layer_A { 
	unsigned char modulation_scheme; // 3
	unsigned char coding_rate_of_inner_code; //3 
	unsigned char length_of_time_interleaving;  //3
	unsigned char number_of_segments;  //4
};

typedef transmission_parameters_for_layer_A transmission_parameters_for_layer_B;
typedef transmission_parameters_for_layer_A transmission_parameters_for_layer_C;


struct MCCI {   //modulation_Control_configuration_information() 
	unsigned char TMCC_synchronization_word; //        1
	unsigned char AC_data_effective_position; //        1
	unsigned char reserved; //                          2
		struct mode_GI_information { 
			unsigned char initialization_timing_indicator; //    4
			unsigned char current_mode; //                    2
			unsigned char current_guard_interval; //2
			unsigned char next_mode; //2
			unsigned char next_guard_interval; //2
		}mgi;

		struct TMCC_information { 
			unsigned char system_identifier; // 2
			unsigned char count_down_index; //4
			unsigned char switch_on_control_flag_used_for_alert_broadcasting; // 1
				struct currrent_configuration_information { 
					transmission_parameters_for_layer_A layerA;
					transmission_parameters_for_layer_B layerB;
					transmission_parameters_for_layer_C layerC;
				}currentConfiguration; 

				struct next_configuration_information { 
					unsigned char partial_reception_flag; // 1
					transmission_parameters_for_layer_A layerA;
					transmission_parameters_for_layer_B layerB;
					transmission_parameters_for_layer_C layerC;
				}nextConfiguration; 
	
			unsigned char phase_correction_of_CP_in_connected_transmission; // 3
			unsigned int TMCC_reserved_future_use; // 12
			unsigned int reserved_future_use; // 10
		}tmccInformation;

	unsigned int CRC_32; //32
};

struct network_synchronization_information { 
	unsigned char synchronization_id; // 8
	unsigned char stuffing_byte[16]; 
};

struct IIP {   
    unsigned int IIP_packet_pointer; //  16 
    MCCI mcci; // 160
    unsigned char IIP_branch_number; // 8
    unsigned char last_IIP_branch_number; // 8 
    unsigned char network_synchronization_information_lenght; // 8 TBD!!!
    network_synchronization_information nsi;  
	unsigned char* stuffing_byte; // TBD SIZE AS BELOW FOR
}; 

struct ISDB_information {  
   unsigned char TMCC_identifier; 
   unsigned char reserved;
   unsigned char buffer_reset_control_flag; 
   unsigned char switch_on_control_flag_for_emergency_broadcasting;
   unsigned char initialization_timing_head_packet_flag;
   unsigned char frame_head_packet_flag;
   unsigned char frame_indicator; 
   unsigned char layer_indicator; 
   unsigned char count_down_index; 
   unsigned char AC_data_invalid_flag; 
   unsigned char AC_data_effective_bytes; 
   unsigned int  TSP_counter; 
   unsigned int stuffing_bit_or_AC_data; 
   unsigned char reed_solomon[8];
}; 