#include "crc.h"



#define CRC32POLY       0x04c11db7

unsigned int calculateCRC(unsigned char* aBuffer, size_t size) {

	unsigned int crc = 0xffffffff;

	unsigned char* tmp = aBuffer;

	while (size--) {

		crc ^= *tmp++ << 24;

		for (unsigned int i = 0; i < 8; i++)

			crc = (crc << 1) ^ ((crc & 0x80000000) ? CRC32POLY : 0);

	}

	return crc;

} 